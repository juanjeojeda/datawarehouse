function filterDropdown(originalFilter, selectElementId, elementsList) {
    const filter = originalFilter.toLowerCase();
    const dropdown = document.getElementById(selectElementId);

    // Remove all options first
    while (dropdown.firstChild)
        dropdown.removeChild(dropdown.firstChild);

    // Add the ones that contain the filter as a substring
    for (let element of elementsList) {
        if (
            element.tag.toLowerCase().indexOf(filter) !== -1 ||
                element.description.toLowerCase().indexOf(filter) !== -1
        ) {
            const option = document.createElement('option');
            option.value = element.id;
            option.innerText = element.description;
            dropdown.appendChild(option);
        }
    }
}

function confirmRemoveIssue(elem) {
    // Confirmation dialog to delete an Issue Occurrence.
    return confirm('Do you really want to remove this issue?');
}

function confirmRemoveBuild(elem) {
    // Confirmation dialog to delete a Build from an Issue Occurrence.
    return confirm('Do you really want to remove this build from the issue?');
}

function confirmRemoveTest(elem) {
    // Confirmation dialog to delete a Test from an Issue Occurrence.
    return confirm('Do you really want to remove this test from the issue?');
}
