# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to actions."""

from django.db import models


class ActionKind(models.Model):
    """Model for ActionKind."""

    name = models.CharField(max_length=50)

    def __str__(self):
        """Return __str__ formatted."""
        return self.name


class Action(models.Model):
    """Model for PipelineAction."""

    created_at = models.DateTimeField(auto_now_add=True)
    kind = models.ForeignKey(ActionKind, on_delete=models.CASCADE)
    pipeline = models.ForeignKey('Pipeline',
                                 related_name='actions',
                                 on_delete=models.CASCADE)

    def __str__(self):
        """Return __str__ formatted."""
        return f'[{self.created_at}] {self.kind.name}'
