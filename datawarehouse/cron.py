# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Cron file."""
from celery import shared_task

from cki_lib.logger import get_logger

from .scripts import update_queued_tests
from .patches import sync_patchwork_series

LOGGER = get_logger(__name__)


def cron_patches():
    """Update patches from Patchwork."""
    LOGGER.info("Running sync_patchwork_series")
    sync_patchwork_series()


def cron_queued():
    """Update tests that are stored as queued."""
    LOGGER.info("Updating queued tests")
    update_queued_tests()


@shared_task
def run():
    """Run cron_ tasks."""
    cron_patches()
    cron_queued()
