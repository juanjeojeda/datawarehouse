"""Test for issues API."""
import json
from unittest import mock

from tests import utils
from datawarehouse import models, serializers


class TestRevisionIssues(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Test issues in revisions."""

    obj = models.KCIDBRevision
    permission_name = 'kcidbrevision'
    kcidb_endpoint = 'kcidb/revisions/decd6167bf4f6bec1284006d0522381b44660df3'

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set up data."""
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {'origin': 'redhat', 'id': 'decd6167bf4f6bec1284006d0522381b44660df3'},
            ],
            'builds': [
                {'origin': 'redhat', 'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1'},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1'},
            ]
        }

        self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        self.issue_1 = models.Issue.objects.create(description='foo bar',
                                                   ticket_url='http://some.url', kind=issue_kind)
        self.issue_2 = models.Issue.objects.create(description='other foo bar',
                                                   ticket_url='http://other.url', kind=issue_kind)

    def test_create_issues(self):
        """Test adding an issue."""
        response = self.assert_authenticated_post(
            201, f'add_{self.permission_name}',
            f'/api/1/{self.kcidb_endpoint}/issues',
            json.dumps({'issue_id': self.issue_1.id}), content_type="application/json")

        self.assertDictEqual(
            serializers.IssueSerializer(self.issue_1).data,
            response.json()
        )

        self.assertEqual(1, self.obj.objects.get().issues.count())
        self.assertEqual(self.issue_1.id, self.obj.objects.get().issues.get().id)

        response = self.assert_authenticated_post(
            201, f'add_{self.permission_name}',
            f'/api/1/{self.kcidb_endpoint}/issues',
            json.dumps({'issue_id': self.issue_2.id}), content_type="application/json")

        self.assertEqual(2, self.obj.objects.get().issues.count())

    def test_get_issue_single(self):
        """Test getting an issue."""
        self.obj.objects.get().issues.add(self.issue_1)
        self.assertEqual(1, self.obj.objects.get().issues.count())

        response_single = self.client.get(
            f'/api/1/{self.kcidb_endpoint}/issues/{self.issue_1.id}',
        )
        self.assertEqual(
            serializers.IssueSerializer(self.issue_1).data,
            response_single.json()
        )

    def test_get_issue_list(self):
        """Test getting issues."""
        self.obj.objects.get().issues.add(self.issue_1, self.issue_2)
        self.assertEqual(2, self.obj.objects.get().issues.count())

        response_list = self.client.get(
            f'/api/1/{self.kcidb_endpoint}/issues',
        )
        self.assertEqual(
            serializers.IssueSerializer([self.issue_2, self.issue_1], many=True).data,
            response_list.json()['results']
        )

    def test_delete_issues(self):
        """Test adding an issue."""
        self.obj.objects.get().issues.add(self.issue_1, self.issue_2)
        self.assertEqual(2, self.obj.objects.get().issues.count())

        self.assert_authenticated_delete(
            204, f'delete_{self.permission_name}',
            f'/api/1/{self.kcidb_endpoint}/issues/{self.issue_1.id}',
            content_type="application/json")

        self.assertEqual(1, self.obj.objects.get().issues.count())
        self.assertEqual(self.issue_2.id, self.obj.objects.get().issues.get().id)


class TestBuildIssues(TestRevisionIssues):
    """Test issues in builds."""

    obj = models.KCIDBBuild
    permission_name = 'kcidbbuild'
    kcidb_endpoint = 'kcidb/builds/redhat:build-1'


class TestTestIssues(TestRevisionIssues):
    """Test issues in tests."""

    obj = models.KCIDBTest
    permission_name = 'kcidbtest'
    kcidb_endpoint = 'kcidb/tests/redhat:test-1'
