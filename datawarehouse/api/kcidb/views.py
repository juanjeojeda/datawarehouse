# pylint: disable=too-few-public-methods
"""Views."""
from collections import defaultdict

from datawarehouse import models, pagination, utils, signals

from django.conf import settings
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key, strtobool

from . import serializers
from datawarehouse import serializers as dw_serializers

LOGGER = get_logger(__name__)
TREES_TO_PUSH = [
    'arm',
    'mainline.kernel.org',
    'net-next',
    'rdma',
    'rt-devel',
    'scsi',
    'upstream-stable',
]


class KCIDBView(APIView):
    """Endpoint for kcidb data handling."""

    queryset = models.Pipeline.objects.none()

    config = {
        'revisions': {
            'model': models.Pipeline,
            'serializer': serializers.RevisionSerializer,
            'filter': {'gittree__name__in': TREES_TO_PUSH},
            'date_filter': 'started_at__gte'
            },
        'builds': {
            'model': models.BuildRun,
            'serializer': serializers.BuildSerializer,
            'filter': {'pipeline__gittree__name__in': TREES_TO_PUSH},
            'date_filter': 'pipeline__started_at__gte'
            },
        'tests': {
            'model': models.BeakerTestRun,
            'serializer': serializers.TestSerializer,
            'filter': {
                'pipeline__gittree__name__in': TREES_TO_PUSH,
                },
            'exclude': {
                'result__name__in': ('SKIP', 'NEW'),
                },
            'date_filter': 'pipeline__started_at__gte',
            },
    }

    def get(self, request, kind):
        """Get a list of items of the type {kind}."""
        if kind not in self.config.keys():
            return HttpResponseBadRequest(f"Not a correct kind. Must be in {list(self.config.keys())}.")

        config = self.config[kind]
        last_retrieved_id = request.GET.get('last_retrieved_id')
        min_pipeline_started_at = request.GET.get('min_pipeline_started_at')

        if not last_retrieved_id:
            return HttpResponseBadRequest("Missing 'last_retrieved_id' parameter.")

        paginator = pagination.DatawarehousePagination()

        items = config['model'].objects.filter(id__gt=last_retrieved_id, **config['filter']).order_by('id')
        if config.get('exclude'):
            items = items.exclude(**config['exclude'])
        if min_pipeline_started_at:
            try:
                min_pipeline_started_at = utils.timestamp_to_datetime(min_pipeline_started_at)
            except ValueError as exception:
                return HttpResponseBadRequest(exception)
            items = items.filter(**{config['date_filter']: min_pipeline_started_at})

        paginated_items = paginator.paginate_queryset(items, request)
        serialized_items = config['serializer'](paginated_items, many=True).data

        # Remove these keys when it's value is None.
        keys_to_remove = ['message_id', 'log_url', 'duration', 'config_url',
                          'compiler', 'path']
        for item in serialized_items:
            for key in keys_to_remove:
                if not item.get(key):
                    item.pop(key, None)

        try:
            last_item_id = paginated_items[-1].id
        except IndexError:
            last_item_id = int(last_retrieved_id)

        context = {
            'data': {
                'version': {'major': 1, 'minor': 1},
                kind: serialized_items,
            },
            'last_retrieved_id': last_item_id
        }

        return paginator.get_paginated_response(context)


class Submit(APIView):
    """Submit KCIDB data."""

    queryset = models.KCIDBRevision.objects.none()

    @staticmethod
    def notify_tests_jobs_finished(data, errors):
        """Notify when test results are available for a pipeline."""
        # Exclude those that were not stored because of errors.
        tested_pipelines = {
            get_nested_key(test, 'misc/pipeline/id')
            for test in data.get('tests', [])
            if not test['id'] in errors.get('tests', {})
        }

        # Remove None returned by get_nested_key when it is not present.
        tested_pipelines.discard(None)

        for pipeline_id in tested_pipelines:
            signals.pipeline_processing_done.send(
                sender='api.kcidb.submit',
                pipeline=models.Pipeline.objects.get(pipeline_id=pipeline_id)
            )

    def post(self, request):
        """Post json data."""
        errors = defaultdict(dict)
        data_models = (
            ('revisions', models.KCIDBRevision),
            ('builds', models.KCIDBBuild),
            ('tests', models.KCIDBTest),
        )

        input_data = request.data.get('data')

        if input_data.get('version') != {'major': 3, 'minor': 0}:
            LOGGER.info('Submitted unknown schema version: %s', input_data.get('version'))
            return HttpResponseBadRequest('Unknown schema version.')

        for key, model in data_models:
            for data in input_data.get(key, []):

                # Do not submit retrigger pipelines unless ALLOW_DEBUG_PIPELINES=True
                is_retrigger = strtobool(get_nested_key(data, 'misc/pipeline/variables/retrigger', 'false'))
                if is_retrigger and not settings.ALLOW_DEBUG_PIPELINES:
                    errors[key][data['id']] = 'Retrigger pipeline ignored'
                    continue

                try:
                    model.create_from_json(data)
                except (models.AlreadySubmitted, models.MissingParent) as error:
                    errors[key][data['id']] = str(error)

        self.notify_tests_jobs_finished(input_data, errors)

        if errors:
            LOGGER.info('Errors on submission: %s', errors)
            return Response({'errors': errors}, status=status.HTTP_201_CREATED)

        return Response(status=status.HTTP_201_CREATED)


class RevisionGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Revision."""

    serializer_class = serializers.KCIDBRevisionSerializer
    queryset = models.KCIDBRevision.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class RevisionList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of Revisions."""

    serializer_class = serializers.KCIDBRevisionSerializer
    queryset = models.KCIDBRevision.objects.all()


class BuildGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Build."""

    serializer_class = serializers.KCIDBBuildSerializer
    queryset = models.KCIDBBuild.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class BuildList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of Builds for a given Revision."""

    serializer_class = serializers.KCIDBBuildSerializer
    queryset = models.KCIDBBuild.objects.all()
    lookup_fields = (
        ('id', 'revision__%%id'),
    )


class TestGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Test."""

    serializer_class = serializers.KCIDBTestSerializer
    queryset = models.KCIDBTest.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class TestList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of Tests run on a given Build."""

    serializer_class = serializers.KCIDBTestSerializer
    queryset = models.KCIDBTest.objects.all()
    lookup_fields = (
        ('id', 'build__%%id'),
    )


class IssueOccurrenceView(APIView):
    """Issue occurrences management."""

    def _get_object(self, id):
        if id.isdigit() and int(id) < 2e9:
            return get_object_or_404(self.model.objects, iid=id)
        else:
            return get_object_or_404(self.model.objects, id=id)

    def get(self, request, id, issue_id=None, **kwargs):
        if issue_id:
            return self._get(request, id, issue_id)
        else:
            return self._list(request, id)

    def _list(self, request, id, issue_id=None):
        obj = self._get_object(id)
        obj_issues = obj.issues.all()

        paginator = pagination.DatawarehousePagination()
        paginated = paginator.paginate_queryset(obj_issues, request)

        serialized = dw_serializers.IssueSerializer(paginated, many=True).data
        return paginator.get_paginated_response(serialized)

    def _get(self, request, id, issue_id=None):
        obj = self._get_object(id)
        obj_issue = get_object_or_404(obj.issues, id=issue_id)

        serialized = dw_serializers.IssueSerializer(obj_issue).data
        return Response(serialized)

    def post(self, request, id):
        """Mark revision issues."""
        issue_id = request.data.get('issue_id')

        kcidb_test = self._get_object(id)
        issue = get_object_or_404(models.Issue, id=issue_id)
        kcidb_test.issues.add(issue)

        serialized = dw_serializers.IssueSerializer(issue).data
        return Response(serialized, status=status.HTTP_201_CREATED)

    def delete(self, request, id, issue_id):
        """Delete Issue relationship."""
        kcidb_test = self._get_object(id)
        issue = get_object_or_404(models.Issue, id=issue_id)
        kcidb_test.issues.remove(issue)

        return Response(status=status.HTTP_204_NO_CONTENT)


class RevisionIssueOccurrence(IssueOccurrenceView):
    """Revision Issue occurrences management."""

    queryset = models.KCIDBRevision.objects.none()
    model = models.KCIDBRevision


class BuildIssueOccurrence(IssueOccurrenceView):
    """Build Issue occurrences management."""

    queryset = models.KCIDBBuild.objects.none()
    model = models.KCIDBBuild


class TestIssueOccurrence(IssueOccurrenceView):
    """Test Issue occurrences management."""

    queryset = models.KCIDBTest.objects.none()
    model = models.KCIDBTest
