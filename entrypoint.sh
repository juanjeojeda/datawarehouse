#! /bin/bash
python3 manage.py migrate
# canonical is_true check, see pipeline-definition
if [[ "${IS_PRODUCTION:-}" = [Tt]rue ]] ; then
    python3 manage.py collectstatic
    supervisord
    tail --follow=name --retry /var/log/nginx/dw-{access,error}.log
else
    supervisord -c /code/configs/supervisor-app.devel.ini
fi
