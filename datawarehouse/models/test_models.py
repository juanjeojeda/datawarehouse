# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to the test stage."""

from django.conf import settings
from django.db import models

from datawarehouse import styles
from . import pipeline_models


class TestMaintainer(models.Model):
    """Model for TestMaintainer."""

    name = models.CharField(max_length=50)
    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'


class TestManager(models.Manager):
    """Natural key for Test."""

    def get_by_natural_key(self, name, fetch_url):
        """Lookup the object by the natural key."""
        return self.get(name=name, fetch_url=fetch_url)


class Test(models.Model):
    """Model for Test."""

    name = models.CharField(max_length=200)
    fetch_url = models.URLField(blank=True, null=True)
    maintainers = models.ManyToManyField(TestMaintainer)
    universal_id = models.CharField(max_length=50, null=True)

    objects = TestManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, self.fetch_url)

    @property
    def confidence(self):
        """Confidence index."""
        result = (self.testrun_set
                  .annotate(passed_count=models.Count('id', filter=models.Q(passed=True)))
                  .aggregate(confidence=models.Avg('passed_count')))['confidence']
        return result if result is not None else 1

    @property
    def name_sanitized(self):
        """Turn a string into a path-compatible name."""
        return "".join(
            char if char.isalnum() else '_' for char in self.name
        )


class BeakerResourceManager(models.Manager):
    """Natural key for BeakerResource."""

    def get_by_natural_key(self, fqdn):
        """Lookup the object by the natural key."""
        return self.get(fqdn=fqdn)


class BeakerResource(models.Model):
    """Model for BeakerResource."""

    fqdn = models.CharField(max_length=100, blank=True, null=True)

    objects = BeakerResourceManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.fqdn}'

    def natural_key(self):
        """Return the natural key."""
        return (self.fqdn, )

    @property
    def confidence(self):
        """Confidence index."""
        result = (self.beakertestrun_set
                  .annotate(passed_count=models.Count('id', filter=models.Q(passed=True)))
                  .aggregate(confidence=models.Avg('passed_count')))['confidence']
        return result if result is not None else 1


class TestResult(models.Model):
    """Model for TestResult."""

    name = models.CharField(max_length=10)

    objects = pipeline_models.GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, )


class TestRun(models.Model):
    """Model for TestRun."""

    pipeline = models.ForeignKey(pipeline_models.Pipeline,
                                 on_delete=models.CASCADE,
                                 related_name='test_jobs')
    test = models.ForeignKey(Test, on_delete=models.PROTECT)
    waived = models.BooleanField()
    kernel_arch = models.ForeignKey(pipeline_models.Architecture, on_delete=models.PROTECT)
    kernel_debug = models.BooleanField(default=False)
    logs = models.ManyToManyField('Artifact')
    passed = models.BooleanField(default=False)
    skipped = models.BooleanField(default=False)
    result = models.ForeignKey(TestResult, on_delete=models.PROTECT)
    targeted = models.BooleanField(default=False)

    kcidb_test = models.ForeignKey('KCIDBTest', on_delete=models.CASCADE, null=True)
    issues = models.ManyToManyField('Issue')

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.pipeline.pipeline_id}: {self.kernel_arch.name} - {self.test.name}'

    @property
    def passed_style(self):
        """Style according to the passed value.

        Is modified if the result was waived or invalid.
        """
        return styles.get_style(self.result.name, waived=self.waived)

    def save(self, *args, **kwargs):
        # pylint: disable=arguments-differ
        """Populate passed, skipped flags."""
        self.passed = self.result.name == 'PASS'
        self.skipped = self.result.name == 'SKIP'

        super(TestRun, self).save(*args, **kwargs)

    @classmethod
    def create_from_misc(cls, test, misc):
        """Create TestRun from kcidb misc field."""
        pipeline = pipeline_models.Pipeline.create_from_misc(misc)

        testrun = cls.objects.update_or_create(
            pipeline=pipeline,
            test=test.test,
            kernel_arch=test.build.architecture,
            defaults={
                'waived': test.waived,
                'kernel_debug': misc.get('debug', False),
                'result': test.status,
                'targeted': misc.get('targeted', False),
                'kcidb_test': test,
            }
        )[0]

        testrun.logs.add(*test.output_files.all())


class BeakerTestRun(TestRun):
    """Model for BeakerTestRun."""

    job = models.ForeignKey(pipeline_models.Job, on_delete=models.PROTECT)
    jid = models.IntegerField()  # Gitlab's Job ID
    task_id = models.IntegerField()  # Beaker Task ID
    recipe_id = models.IntegerField()  # Beaker Recipe ID
    beaker_resource = models.ForeignKey(BeakerResource, on_delete=models.PROTECT)
    started_at = models.DateTimeField(null=True)
    finished_at = models.DateTimeField(null=True)
    duration = models.IntegerField(null=True)
    retcode = models.IntegerField()

    @property
    def recipe_url(self):
        """Return formatted recipe URL."""
        return f'{settings.BEAKER_URL}/recipes/{self.recipe_id}'

    @property
    def trace_url(self):
        """Return URL to trace file."""
        return (
            f'{settings.GITLAB_URL}/api/v4/projects/{self.pipeline.project.project_id}'
            f'/jobs/{self.jid}/trace'
        )

    @classmethod
    def create_from_misc(cls, test, misc):
        """Create BeakerTestRun from kcidb misc field."""
        pipeline = pipeline_models.Pipeline.create_from_misc(misc)

        stage = pipeline_models.Stage.objects.get_or_create(name=misc['job']['stage'])[0]
        job = pipeline_models.Job.objects.get_or_create(name=misc['job']['name'], stage=stage)[0]

        testrun = cls.objects.update_or_create(
            job=job,
            jid=misc['job']['id'],
            task_id=misc['beaker']['task_id'],
            recipe_id=misc['beaker']['recipe_id'],
            pipeline=pipeline,
            test=test.test,
            defaults={
                'waived': test.waived,
                'kernel_arch': test.build.architecture,
                'beaker_resource': test.environment,
                'started_at': test.start_time,
                'duration': test.duration,
                'passed': test.status.name == 'PASS',
                'result': test.status,
                'retcode': misc['beaker']['retcode'],
                'finished_at': misc['beaker']['finish_time'],
                'kernel_debug': misc['debug'],
                'targeted': misc['targeted'],
                'kcidb_test': test,
            }
        )[0]

        testrun.logs.add(*test.output_files.all())
