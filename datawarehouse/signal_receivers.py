"""Receiver functions for signals."""
from django import dispatch
from django.utils import timezone

from datawarehouse import utils, models, signals
from datawarehouse.api.kcidb import serializers as kcidb_serializers


@dispatch.receiver(signals.pipeline_processing_done)
def send_pipeline_message(**kwargs):
    """Send pipeline message."""
    pipeline = kwargs['pipeline']

    message = {
        'timestamp': timezone.now().isoformat(),
        'status': 'finished' if pipeline.finished_at else 'running',
        'pipeline_id': pipeline.pipeline_id,
    }
    utils.send_pipeline_notification(message)


@dispatch.receiver(signals.kcidb_object)
def send_kcidb_object_message(**kwargs):
    """Send new kcidb object message."""
    serializers = {
        'revision': kcidb_serializers.KCIDBRevisionSerializer,
        'build': kcidb_serializers.KCIDBBuildSerializer,
        'test': kcidb_serializers.KCIDBTestSerializer,
    }

    status = kwargs['status']
    object_type = kwargs['object_type']
    object_instance = kwargs['object_instance']

    message = {
        'timestamp': timezone.now().isoformat(),
        'status': status,
        'object_type': object_type,
        'object': serializers[object_type](object_instance).data,
        'id': object_instance.id,
        'iid': object_instance.iid,
    }
    utils.send_kcidb_notification(message)


@dispatch.receiver(signals.action_created)
def send_ready_to_report_message(**kwargs):
    """Send message to inform that a pipeline is ready to be reported."""
    action = kwargs['action']
    pipeline = action.pipeline
    triaged_kind, _ = models.ActionKind.objects.get_or_create(name='triaged')

    # Condition: The pipeline was just triaged by the first time.
    if action.kind == triaged_kind and pipeline.actions.filter(kind=triaged_kind).count() == 1:
        message = {
            'timestamp': timezone.now().isoformat(),
            'status': 'ready_to_report',
            'pipeline_id': pipeline.pipeline_id,
        }
        utils.send_pipeline_notification(message)
