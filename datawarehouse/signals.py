"""Signals."""
from django import dispatch


# pylint: disable=invalid-name
pipeline_processing_done = dispatch.Signal(providing_args=["pipeline"])
action_created = dispatch.Signal(providing_args=["action"])
kcidb_object = dispatch.Signal(providing_args=["status", "object_type", "object_instance"])
