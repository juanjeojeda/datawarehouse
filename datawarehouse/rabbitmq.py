"""RabbitMQ messaging implementation."""
import json
import threading

import pika
from cki_lib import messagequeue
from cki_lib.logger import get_logger

from datawarehouse import models

LOGGER = get_logger(__name__)
MESSAGE_PENDING_LOCK = threading.Lock()


class MessageQueue:
    """Messages Queue."""

    def __init__(self, host, port, user, password):
        """Init."""
        self.queue = messagequeue.MessageQueue(host, port, user, password)

    def send(self):
        """Send messages in the queue."""
        with MESSAGE_PENDING_LOCK:
            for message in models.MessagePending.objects.all():
                data = json.loads(message.body)
                try:
                    self.queue.send_message(data=data, queue_name='', exchange=message.exchange)
                except pika.exceptions.AMQPError:
                    LOGGER.exception('Error sending message, will be retried')
                    break
                else:
                    message.delete()

    @staticmethod
    def add(data, exchange):
        """Add message to the queue."""
        body = json.dumps(data)
        models.MessagePending.objects.create(body=body, exchange=exchange)
