"""Test the scripts.metrics module."""
import datetime
from django.utils import timezone
from pytz import UTC

from tests import utils
from tests.test_patches import mock_series

from datawarehouse import models, patches, scripts


class MetricsTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the metrics stuff."""

    def setUp(self):
        """Set up tests."""
        self.project = models.Project.objects.create(project_id=0)
        self.git_trees = [
            models.GitTree.objects.create(name='Tree 1'),
            models.GitTree.objects.create(name='Tree 2'),
        ]
        self.pipelines = [
            models.Pipeline.objects.create(pipeline_id=1, project=self.project,
                                           gittree=self.git_trees[0],
                                           created_at=datetime.datetime(2019, 10, 14, 0, 0, tzinfo=UTC)),
            models.Pipeline.objects.create(pipeline_id=2, project=self.project,
                                           gittree=self.git_trees[1],
                                           created_at=datetime.datetime(2019, 10, 14, 0, 0, tzinfo=UTC)),
        ]
        self.hosts = [
            models.BeakerResource.objects.create(id=1, fqdn='host_1'),
            models.BeakerResource.objects.create(id=2, fqdn='host_2'),
        ]
        self.kernel_arch = models.Architecture.objects.create(name='arch')
        self.tests = [
            models.Test.objects.create(id=1, name='test_1'),
            models.Test.objects.create(id=2, name='test_2'),
        ]
        self.results = [
            models.TestResult.objects.create(name='FAIL'),
            models.TestResult.objects.create(name='PASS'),
        ]
        self.stage = models.Stage.objects.create(name='stage')
        self.jobs = [
            models.Job.objects.create(name='job_1', stage=self.stage),
            models.Job.objects.create(name='job_2', stage=self.stage),
            models.Job.objects.create(name='job_3', stage=self.stage),
            models.Job.objects.create(name='job_4', stage=self.stage),
            ]

        # pylint: disable=line-too-long
        tests_list = [
            (0, self.jobs[0], self.pipelines[0], self.tests[0], self.results[1], self.hosts[1]), # noqa
            (1, self.jobs[1], self.pipelines[0], self.tests[1], self.results[0], self.hosts[0]), # noqa
            (2, self.jobs[2], self.pipelines[1], self.tests[0], self.results[1], self.hosts[0]), # noqa
            (3, self.jobs[3], self.pipelines[1], self.tests[1], self.results[0], self.hosts[1]), # noqa
        ]

        self.test_runs = []
        for incremental_id, job, pipeline, test_, result, host in tests_list:
            models.BeakerTestRun.objects.create(
                job=job, jid=incremental_id, task_id=incremental_id, recipe_id=incremental_id,
                pipeline=pipeline, test=test_, result=result,
                waived=False, kernel_arch=self.kernel_arch, beaker_resource=host, retcode=0
            )

        series = mock_series()
        patches.store_patchwork_series(series)
        for patch in models.Patch.objects.all():
            patch.pipelines.add(self.pipelines[0])

    def test_get_series_metrics(self):
        """Test get_series_metrics."""
        date_from = timezone.now() - timezone.timedelta(days=9999)
        trees = models.GitTree.objects.filter(name='Tree 1')

        metrics = scripts.get_series_metrics(trees, date_from)

        self.assertContextEqual(
            metrics,
            {'title': 'Series Details',
             'plots': [
                 {'type': 'area', 'title': 'Total / Targeted patch series',
                  'element_id': 'plot_1', 'x_axis': 'date',
                  'values': [
                      {'date': datetime.datetime(2019, 10, 14, 0, 0, tzinfo=UTC),
                       'targeted_percent': 0,
                       'total': 1,
                       'total_targeted': 0}
                  ], 'y_axis': [
                      {'id': 'A', 'label': '# Pipelines per week', 'position': 'left', 'lines': [
                          {'label': 'Targeted #', 'value': 'total_targeted',
                           'bg_color': '#ac880a10', 'border_color': '#ac880a15'},
                          {'label': 'Total #', 'value': 'total',
                           'bg_color': '#2a188a10', 'border_color': '#2a188a15'},
                          ]
                       },
                      {'id': 'B', 'label': 'Percent', 'position': 'right', 'append': '%', 'lines': [
                          {'label': 'Targeted %', 'value': 'targeted_percent',
                           'bg_color': '#00000000', 'border_color': '#1cc88a'},
                          ]
                       }
                      ]
                  }
                 ],
             }
            )

    def test_get_pipeline_metrics(self):
        """Test get_pipelines_metrics."""
        date_from = timezone.now() - timezone.timedelta(days=9999)
        trees = models.GitTree.objects.filter(name='Tree 1')

        metrics = scripts.get_pipelines_metrics(trees, date_from)

        self.assertContextEqual(
            metrics,
            {'title': 'Pipelines Details',
             'plots': [
                 {'color': scripts.metrics.COLORS, 'hover_color': scripts.metrics.HOVER_COLORS,
                  'element_id': 'plot_2',
                  'labels': ['Lint', 'Merge', 'Build', 'Test', 'Passed'],
                  'size': 12,
                  'title': 'Pipeline Issues',
                  'type': 'doughnut',
                  'values': [0, 0, 0, 1, 0],
                  'description': 'Distribution of failures through pipeline stages.'
                  }
             ],
             }
        )

    def test_get_tests_metrics(self):
        """Test get_tests_metrics."""
        date_from = timezone.now() - timezone.timedelta(days=9999)
        trees = models.GitTree.objects.filter(name='Tree 1')

        metrics = scripts.get_tests_metrics(trees, date_from)

        self.assertContextEqual(
            metrics,
            {'title': 'Test Issue Details', 'plots': [
                {'color': ['#00000010']+scripts.metrics.COLORS,
                 'hover_color': ['#00000040']+scripts.metrics.HOVER_COLORS,
                 'element_id': 'plot_4',
                 'labels': ['Not Tagged'],
                 'title': 'Issue Records',
                 'type': 'doughnut',
                 'description': (
                     'Number of failed pipelines categorized by issue kind. ' +
                     'If a pipeline contains multiple Issue Records for the same Issue Kind, ' +
                     'only one ocurrence is counted.'
                 ),
                 'values': [1]},
                {'color': scripts.metrics.COLORS, 'hover_color': scripts.metrics.HOVER_COLORS,
                 'element_id': 'plot_5',
                 'labels': ['test_2', 'test_1'],
                 'title': 'Top Failed Tests',
                 'type': 'doughnut',
                 'description': (
                     'Number of failed pipelines categorized by test. ' +
                     'If a pipeline contains multiple Issue Records for the same Issue Kind, ' +
                     'only one ocurrence is counted.'
                 ),
                 'values': [1, 0]},
                {'color': scripts.metrics.COLORS, 'hover_color': scripts.metrics.HOVER_COLORS,
                 'element_id': 'plot_6',
                 'labels': ['arch'],
                 'title': 'Top Failed Architectures',
                 'type': 'doughnut',
                 'description': (
                     'Number of failed pipelines categorized by architecture. ' +
                     'If a pipeline contains multiple Issue Records for the same Issue Kind, ' +
                     'only one ocurrence is counted.'
                 ),
                 'values': [1]
                 },
                {'color': scripts.metrics.COLORS, 'hover_color': scripts.metrics.HOVER_COLORS,
                 'element_id': 'plot_unique_issues',
                 'labels': [],
                 'title': 'Unique Issues per Kind',
                 'type': 'doughnut',
                 'description': 'Note: Records from generic Issues are counted individually.',
                 'values': []
                 },
                {'type': 'doughnut',
                 'title': 'Generic Issues Records',
                 'element_id': 'plot_generics',
                 'labels': [],
                 'values': [],
                 'color': scripts.metrics.COLORS,
                 'hover_color': scripts.metrics.HOVER_COLORS,
                 'description': 'Number of Issue Records per Generic Issue.'
                 },
                {'element_id': 'plot_issues_time',
                 'title': 'Issues and Issue Records',
                 'size': 6,
                 'type': 'area',
                 'values': [],
                 'x_axis': 'date',
                 'y_axis': [{'id': 'A',
                             'label': '# Per week',
                             'lines': [{'bg_color': '#ac880a05',
                                        'border_color': '#ac880a8a',
                                        'label': 'Issues',
                                        'value': 'issue_count'},
                                       {'bg_color': '#A1364705',
                                        'border_color': '#A136478a',
                                        'label': 'Issue Records',
                                        'value': 'issue_records_count'}],
                             'position': 'left'}]},
                {'element_id': 'plot_issues_kinds_records_time',
                 'title': 'Issue Records per kind',
                 'size': 6,
                 'description': (
                     'If a pipeline contains multiple Issue Records for the same Issue Kind, ' +
                     'only one ocurrence is counted.'
                 ),
                 'type': 'area',
                 'values': [],
                 'x_axis': 'date',
                 'y_axis': [{'id': 'A', 'label': '# Per week', 'lines': [], 'position': 'left'}]
                 },
                ],
             }
        )

    def test_view(self):
        """Test metrics view."""
        response = self.client.get('/metrics/Tree 1')
        self.assertEqual(200, response.status_code)

        trees = models.GitTree.objects.filter(name='Tree 1')
        date_from = timezone.now() - timezone.timedelta(days=9999)

        self.assertContextEqual(response.context, {
            'tree': self.git_trees[0].name,
            'trees': models.GitTree.objects.all().order_by('name'),
            'groups': [
                scripts.get_series_metrics(trees, date_from),
                scripts.get_pipelines_metrics(trees, date_from),
                scripts.get_tests_metrics(trees, date_from),
                ],
            'days_filter_available': {'all': 9999, '3 months': 90, '1 month': 30, '2 weeks': 15, '1 week': 7},
            'days_filter': 'all',
        })
