from django.template import Library

from datawarehouse import styles


register = Library()


@register.filter
def get_style(result):
    if result is True:
        result = 'PASS'
    elif result is False:
        result = 'FAIL'

    return styles.get_style(result)
