"""Django settings for datawarehouse project."""
import os
from socket import gethostname, gethostbyname
from urllib.parse import urlparse

from cki_lib import misc

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SECRET_KEY = os.getenv('SECRET_KEY')

ALLOW_DEBUG_PIPELINES = misc.get_env_bool('ALLOW_DEBUG_PIPELINES', False)
IS_PRODUCTION = misc.is_production()
CSRF_COOKIE_SECURE = IS_PRODUCTION
CORS_ORIGIN_ALLOW_ALL = IS_PRODUCTION
DEBUG = not IS_PRODUCTION
SESSION_COOKIE_SECURE = IS_PRODUCTION

# Some webservers don't support <hostname>//<path>, so make sure that
# base URLs doesn't end in "/"
BEAKER_URL = os.environ.get('BEAKER_URL', '').rstrip('/')
DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL', '').rstrip('/')
GITLAB_URL = os.getenv('GITLAB_URL', '').rstrip('/')
PATCHWORK_URL = os.getenv('PATCHWORK_URL', '').rstrip('/')

REQUESTS_MAX_WORKERS = os.environ.get("REQUESTS_MAX_WORKERS", 10)
REQUESTS_MAX_RETRIES = os.environ.get("REQUESTS_MAX_RETRIES", 3)
REQUESTS_TIMEOUT_S = os.environ.get("REQUESTS_TIMEOUT_S", 60)
REQUESTS_TIME_BETWEEN_RETRIES_S = os.environ.get("REQUESTS_TIME_BETWEEN_RETRIES_S", 2)  # noqa

ALLOWED_HOSTS = [
    urlparse(os.getenv('DATAWAREHOUSE_URL', 'http://*')).netloc,
    gethostname(),
    gethostbyname(gethostname()),
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'datawarehouse',
    'rest_framework',
    'rest_framework.authtoken',
    'django_extensions',
    'corsheaders',
    'django_cleanup.apps.CleanupConfig',
    'debug_toolbar',
]

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

ROOT_URLCONF = 'datawarehouse.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'datawarehouse.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'datawarehouse',
        'USER': 'datawarehouse',
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': 5432,
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',  # noqa
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = '/code/static'

MEDIA_PATH = '/staticfiles/'
MEDIA_URL = f'{DATAWAREHOUSE_URL}{MEDIA_PATH}'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'staticfiles')

if IS_PRODUCTION:
    # the DSN is read from SENTRY_DSN
    sentry_sdk.init(
        ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
        integrations=[DjangoIntegration()]
    )


def show_toolbar(request):
    """Show the toolbar when requested with the debug parameter."""
    return (not request.is_ajax() and
            (request.GET.get('debug') or request.path.startswith('/__debug__/')))


DEBUG_TOOLBAR_CONFIG = {'SHOW_TOOLBAR_CALLBACK': 'datawarehouse.settings.show_toolbar'}

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 30,
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],
    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.JSONParser',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'datawarehouse.api.permissions.DjangoModelPermissionOrReadOnly',
    ]
}

SHELL_PLUS_PRINT_SQL_TRUNCATE = 10000
SHELL_PLUS_PRE_IMPORTS = (
    ('datawarehouse', 'models'),
)

LOGIN_REDIRECT_URL = '/dashboard'
LOGOUT_REDIRECT_URL = '/dashboard'

# RabbitMQ server configuration
RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', '').rstrip('/')
RABBITMQ_PORT = misc.get_env_int('RABBITMQ_PORT', 5672)
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD', 'guest')
RABBITMQ_EXCHANGE_PIPELINES_NOTIFICATIONS = 'pipelines'
RABBITMQ_EXCHANGE_KCIDB_NOTIFICATIONS = os.environ.get('DATAWAREHOUSE_EXCHANGE_KCIDB',
                                                       'cki.exchange.datawarehouse.kcidb')
RABBITMQ_CONFIGURED = bool(RABBITMQ_HOST)

#
# Celery configuration
#
# If RabbitMQ is configured, use it as broker
# Otherwise, fallback to file system backend
if RABBITMQ_CONFIGURED:
    CELERY_BROKER_USE_SSL = {
        'server_hostname': RABBITMQ_HOST
    }
    CELERY_BROKER_URL = f'amqp://{RABBITMQ_USER}:{RABBITMQ_PASSWORD}@{RABBITMQ_HOST}:{RABBITMQ_PORT}'
else:
    CELERY_BROKER_TRANSPORT_OPTIONS = {
        'data_folder_in': '/tmp',
        'data_folder_out': '/tmp',
    }
    CELERY_BROKER_URL = 'filesystem://'

#
# django-influxdb-metrics configuration
#
INFLUXDB_HOST = os.environ.get('INFLUXDB_HOST')
INFLUXDB_PORT = os.environ.get('INFLUXDB_PORT')
INFLUXDB_USER = os.environ.get('INFLUXDB_USER')
INFLUXDB_PASSWORD = os.environ.get('INFLUXDB_PASSWORD')
INFLUXDB_DATABASE = os.environ.get('INFLUXDB_METRICS_DATABASE')
INFLUXDB_TAGS_HOST = 'datawarehouse'
INFLUXDB_TIMEOUT = 5
INFLUXDB_USE_CELERY = True
INFLUXDB_USE_THREADING = False  # Should be True if not using Celery
INFLUXDB_CONFIGURED = bool(INFLUXDB_HOST)

# Only enable django-influxdb-metrics when running on production environments and
# InfluxDB is configured
if IS_PRODUCTION and INFLUXDB_CONFIGURED:
    INSTALLED_APPS.append('influxdb_metrics')
    # Add middleware at the top to cover include all the function calls.
    MIDDLEWARE.insert(
        0, 'influxdb_metrics.middleware.InfluxDBRequestMiddleware'
    )

TIMER_MSG_QUEUE_SEND_PERIOD_S = misc.get_env_int('TIMER_MSG_QUEUE_SEND_PERIOD_S', 10)
