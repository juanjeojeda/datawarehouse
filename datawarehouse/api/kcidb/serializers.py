# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# pylint: disable=no-self-use
"""Serializers."""
import hashlib
import re
from urllib.parse import urlparse
from rest_framework import serializers

from datawarehouse import models

VERSION = {'major': 3, 'minor': 0}


def get_origin_id(pipeline):
    """Format a unique revision origin ID from its parts."""
    revision_origin_id = pipeline.trigger_variables['commit_hash']
    if pipeline.patches.count():
        revision_origin_id += \
            "+" + \
            hashlib.sha256(
                b''.join([patch.url.encode("utf-8") + b'\x00'
                          for patch in pipeline.patches.all()])
            ).hexdigest()
    return revision_origin_id


class KCIDBArtifactSerializer(serializers.ModelSerializer):
    """Serializer for Artifact model. KCIDB style."""

    class Meta:
        """Metadata."""

        model = models.Artifact
        fields = ('name', 'url',)


class RevisionSerializer(serializers.ModelSerializer):
    """Serializer for Revisions."""

    origin = serializers.CharField(default="redhat")
    git_repository_url = serializers.CharField(source='trigger_variables.git_url')
    git_repository_commit_hash = serializers.CharField(source='trigger_variables.commit_hash')
    git_repository_branch = serializers.CharField(source='trigger_variables.branch')
    patch_mboxes = serializers.SerializerMethodField()
    origin_id = serializers.SerializerMethodField()
    valid = serializers.BooleanField(source='merge_passed')
    misc = serializers.SerializerMethodField()
    message_id = serializers.SerializerMethodField()
    contacts = serializers.SerializerMethodField()
    discovery_time = serializers.DateTimeField(source='created_at')
    log_url = serializers.SerializerMethodField()

    def get_message_id(self, pipeline):
        """Get message id."""
        return pipeline.trigger_variables.get('message_id', '')

    def get_patch_mboxes(self, pipeline):
        """Get patch mboxes."""
        return [
            {
                'url': patch.url,
                'name': re.search("([^/]+)/?$", urlparse(patch.url).path)[1]
            } for patch in pipeline.patches.all()
        ]

    def get_origin_id(self, pipeline):
        """Get origin_id."""
        return get_origin_id(pipeline)

    def get_misc(self, pipeline):
        """Get misc."""
        return {'pipeline_id': pipeline.pipeline_id}

    def get_contacts(self, pipeline):
        """Get contacts."""
        try:
            return pipeline.trigger_variables.get('mail_to').split(', ')
        except AttributeError:
            return []

    def get_log_url(self, pipeline):
        if pipeline.merge_jobs.exclude(log=None).exists():
            return pipeline.merge_jobs.get().log.url
        return None

    class Meta:
        """Metadata."""

        model = models.Pipeline
        fields = (
            'origin',
            'git_repository_url', 'git_repository_commit_hash', 'git_repository_branch',
            'patch_mboxes', 'origin_id', 'valid', 'misc', 'message_id', 'contacts', 'discovery_time', 'log_url',
        )


class BuildSerializer(serializers.ModelSerializer):
    """Serializer for Build."""

    revision_origin = serializers.CharField(default="redhat")
    revision_origin_id = serializers.SerializerMethodField()
    origin = serializers.CharField(default="redhat")
    origin_id = serializers.CharField(source='jid')
    architecture = serializers.CharField(source='kernel_arch.name')
    command = serializers.SerializerMethodField()
    valid = serializers.BooleanField(source='success')
    misc = serializers.SerializerMethodField()
    config_name = serializers.CharField(default='fedora')
    config_url = serializers.SerializerMethodField()
    output_files = serializers.SerializerMethodField()
    start_time = serializers.DateTimeField(source='pipeline.started_at')
    log_url = serializers.SerializerMethodField()
    duration = serializers.IntegerField()
    compiler = serializers.CharField()

    def get_revision_origin_id(self, buildrun):
        """Get revision origin_id."""
        return get_origin_id(buildrun.pipeline)

    def get_command(self, buildrun):
        """Get command."""
        return f'make {buildrun.make_opts}'

    def get_misc(self, buildrun):
        """Get misc."""
        return {
            'pipeline_id': buildrun.pipeline.pipeline_id,
            'job_id': buildrun.jid
        }

    def get_config_url(self, buildrun):
        """Get config_url."""
        try:
            return buildrun.artifacts.get(name__endswith='.config').url
        except models.Artifact.DoesNotExist:
            return None

    def get_output_files(self, buildrun):
        """Get output_files."""
        try:
            return [{
                'name': 'kernel.tar.gz',
                'url': buildrun.artifacts.get(name__contains='kernel', name__endswith='.tar.gz').url
            }]
        except models.Artifact.DoesNotExist:
            return []

    def get_log_url(self, buildrun):
        """Get the url of the log."""
        try:
            return buildrun.log.url
        except (AttributeError, ValueError):
            return None

    class Meta:
        """Metadata."""

        model = models.BuildRun
        fields = (
            'revision_origin', 'revision_origin_id', 'origin', 'origin_id',
            'architecture', 'command', 'valid', 'misc', 'output_files',
            'start_time', 'log_url', 'duration', 'config_name', 'config_url',
            'compiler',
        )


class TestSerializer(serializers.ModelSerializer):
    """Serializer for Test."""

    build_origin = serializers.CharField(default="redhat")
    build_origin_id = serializers.SerializerMethodField()
    origin = serializers.CharField(default="redhat")
    origin_id = serializers.CharField(source='task_id')
    description = serializers.CharField(source='test.name')
    status = serializers.CharField(source='result.name')
    start_time = serializers.DateTimeField(source='started_at')
    output_files = KCIDBArtifactSerializer(source='logs', many=True)
    path = serializers.CharField(source='test.universal_id')
    misc = serializers.SerializerMethodField()

    def get_misc(self, testrun):
        """Get misc."""
        return {
            'pipeline_id': testrun.pipeline.pipeline_id,
            'job_id': testrun.jid,
            'beaker_task_id': testrun.task_id,
            'beaker_recipe_id': testrun.recipe_id,
        }

    def get_build_origin_id(self, testrun):
        """Get build origin_id."""
        buildrun = models.BuildRun.objects.get(pipeline=testrun.pipeline, kernel_arch=testrun.kernel_arch)
        return str(buildrun.jid)

    def to_representation(self, instance):
        result = super().to_representation(instance)
        for optional_field in ('duration', 'start_time'):
            if result[optional_field] is None:
                del result[optional_field]
        return result

    class Meta:
        """Metadata."""

        model = models.BeakerTestRun
        fields = (
            'build_origin', 'build_origin_id', 'origin', 'origin_id',
            'description', 'status', 'waived', 'duration', 'start_time', 'output_files',
            'path', 'misc',
        )


class NoEmptyFieldSerializer(serializers.ModelSerializer):
    """Don't serialize empty fields."""

    @staticmethod
    def _value_is_empty(value):
        """Return True if the value is empty."""
        return (
            value is None or
            value == {} or
            value == []
        )

    def to_representation(self, instance):
        result = super().to_representation(instance)
        return {
            key: value for key, value in result.items()
            if not self._value_is_empty(value)
        }


class KCIDBPatchMBOXSerializer(serializers.ModelSerializer):
    """Serializer for Patch KCIDB styled."""
    name = serializers.CharField(source='subject', read_only=True)

    class Meta:
        """Metadata."""

        model = models.Patch
        fields = ('name', 'url')


class KCIDBFileSerializer(serializers.ModelSerializer):
    """Serializer for File KCIDB styled."""

    class Meta:
        """Metadata."""

        model = models.Artifact
        fields = ('url', 'name')


class KCIDBRevisionSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBRevision."""
    origin = serializers.CharField(source='origin.name')
    tree_name = serializers.CharField(source='tree.name', read_only=True)
    patch_mboxes = KCIDBPatchMBOXSerializer(many=True, source='patches')
    log_url = serializers.CharField(source='log.url', read_only=True)
    contacts = serializers.SerializerMethodField()
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBRevision
        fields = ('id', 'origin', 'tree_name',
                  'git_repository_url', 'git_repository_branch',
                  'git_commit_hash', 'git_commit_name',
                  'patch_mboxes', 'message_id', 'description',
                  'publishing_time', 'discovery_time', 'log_url',
                  'contacts', 'valid', 'misc',
                  )

    def get_contacts(self, revision):
        """Return contacts as a flat list of emails."""
        return list(revision.contacts.values_list('email', flat=True))

    def get_misc(self, revision):
        """Return misc field."""
        return {
            'kcidb': {'version': VERSION},
            'iid': revision.iid,
            'is_public': revision.is_public,
        }


class KCIDBBuildSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBBuild."""
    revision_id = serializers.CharField(source='revision.id')
    origin = serializers.CharField(source='origin.name')
    architecture = serializers.CharField(source='architecture.name', read_only=True)
    log_url = serializers.CharField(source='log.url', read_only=True)
    compiler = serializers.CharField(source='compiler.name', read_only=True)
    input_files = KCIDBFileSerializer(many=True)
    output_files = KCIDBFileSerializer(many=True)
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBBuild
        fields = ('revision_id', 'id', 'origin', 'description',
                  'start_time', 'duration', 'architecture',
                  'command', 'compiler', 'input_files', 'output_files', 'config_name',
                  'config_url', 'log_url', 'valid', 'misc',
                  )

    def get_misc(self, build):
        """Return misc field."""
        return {
            'kcidb': {'version': VERSION},
            'iid': build.iid,
            'is_public': build.is_public,
        }


class EnvironmentSerializer(serializers.ModelSerializer):
    """Serializer for Environment (BeakerResource)."""

    description = serializers.CharField(source='fqdn')

    class Meta:
        """Metadata."""

        model = models.BeakerResource
        fields = ('description', )


class KCIDBTestSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBTest."""

    build_id = serializers.CharField(source='build.id')
    origin = serializers.CharField(source='origin.name')
    environment = EnvironmentSerializer()
    description = serializers.CharField(source='test.name', read_only=True)
    path = serializers.CharField(source='test.universal_id', read_only=True)
    status = serializers.CharField(source='status.name', read_only=True)
    output_files = KCIDBFileSerializer(many=True)
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBTest
        fields = ('build_id', 'id', 'origin', 'environment',
                  'path', 'description', 'status', 'waived',
                  'start_time', 'duration', 'output_files', 'misc',
                  )

    def get_misc(self, test):
        """Return misc field."""
        misc = {
            'kcidb': {'version': VERSION},
            'iid': test.iid,
            'is_public': test.is_public,
        }

        try:
            testrun = test.testrun_set.get()
            beakertestrun = testrun.beakertestrun
            misc.update({
                'beaker': {
                    'task_id': beakertestrun.task_id,
                    'recipe_id': beakertestrun.recipe_id,
                }
            })
        except (models.TestRun.DoesNotExist):
            pass

        return misc
