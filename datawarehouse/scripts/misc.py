# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Misc scripts file."""
from django.conf import settings

from cki_lib.logger import get_logger

from datawarehouse import models
from datawarehouse.beaker import Beaker

LOGGER = get_logger(__name__)


def update_queued_tests():
    """Fix tests left in queued because the Gitlab job timeouted."""
    beaker = Beaker(settings.BEAKER_URL)
    incomplete_recipes = models.BeakerTestRun.objects.filter(result__name='NEW').distinct('recipe_id')

    for testrun in incomplete_recipes:
        recipe = beaker.get_recipe(testrun.recipe_id)
        for task in recipe['tasks']:
            try:
                testrun = models.BeakerTestRun.objects.get(task_id=task['id'])
            except models.BeakerTestRun.DoesNotExist:
                continue

            if task['result'] == 'New':
                # Still running
                continue

            if task['status'] == 'Aborted' and not recipe.get('resource'):
                LOGGER.error("Task %s aborted and has no beaker_resource. Deleting.", task['id'])
                testrun.delete()
                continue

            result = models.TestResult.objects.get_or_create(
                name=beaker.get_task_result(task)
            )[0]
            beaker_resource, _ = models.BeakerResource.objects.get_or_create(
                fqdn=recipe['resource']['fqdn']
            )

            testrun.result = result
            testrun.beaker_resource = beaker_resource
            testrun.save()
