"""Test the styles script."""

from django import test
from datawarehouse import styles


class StylesTestCase(test.TestCase):
    """Unit tests for the styles script."""

    def test_styles(self):
        """Test that it returns the necessary keys."""
        levels = ['PASS', 'SKIP', 'FAIL', 'ERROR', 'Running']

        for level in levels:
            style = styles.get_style(level)
            self.assertIsInstance(style, styles.Style)

    def test_waived(self):
        """Test waived affects the result."""
        self.assertNotEqual(
            styles.get_style('FAIL', waived=False),
            styles.get_style('FAIL', waived=True),
        )

        self.assertNotEqual(
            styles.get_style('ERROR', waived=False),
            styles.get_style('ERROR', waived=True),
        )

    def test_fail_error(self):
        """Test fail and error return the same style."""
        self.assertEqual(
            styles.get_style('FAIL'),
            styles.get_style('ERROR'),
        )

        self.assertEqual(
            styles.get_style('FAIL', waived=True),
            styles.get_style('ERROR', waived=True),
        )
