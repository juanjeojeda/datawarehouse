# Generated by Django 2.2.7 on 2019-11-28 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0054_increase_test_name_length'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testrun',
            name='passed',
            field=models.BooleanField(default=False),
        ),
    ]
