# Generated by Django 3.0.5 on 2020-06-04 15:44

from django.db import migrations


def apply_invalid_on_result(apps, schema_editor):
    """Set SKIP result on tests that were marked as invalid_result."""
    db_alias = schema_editor.connection.alias
    TestResultMgr = apps.get_model('datawarehouse', 'TestResult').objects.using(db_alias)
    BeakerTestRunMgr = apps.get_model('datawarehouse', 'BeakerTestRun').objects.using(db_alias)

    result_skip = TestResultMgr.get_or_create(name='SKIP')[0]
    BeakerTestRunMgr.filter(invalid_result=True).update(result=result_skip)


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0093_remove_beaker_result_status'),
    ]

    operations = [
        migrations.RunPython(apply_invalid_on_result),
    ]
