"""Test the views module."""
import json
from unittest import mock

import kcidb

from datawarehouse import models
from datawarehouse.utils import timestamp_to_datetime as ttd
from datawarehouse.api.kcidb import views, serializers
from tests import utils


def create_pipeline_trigger_variables(pipeline, **kwargs):
    """
    Create a pipeline's trigger variable instance.

    Args:
        pipeline:   Pipeline object.
        **kwargs:   Dictionary of the variables and their values to be
                    created.

    Returns:
        A list of created trigger variable objects.

    """
    return [
        models.TriggerVariable.objects.create(
            pipeline=pipeline, key=key, value=value
        ) for key, value in kwargs.items()
    ]


def add_patches(pipeline):
    """Add Patch to a pipeline fron the variables."""
    for url in pipeline.trigger_variables['patch_urls'].split(' '):
        patch, _ = models.Patch.objects.get_or_create(url=url, subject=url)
        pipeline.patches.add(patch)


class FetchTestCase(utils.TestCase):
    """General fetch test case."""

    def setUp(self):
        """Set up tests."""
        # Let's make it work now, refactor later
        # pylint: disable=too-many-locals,too-many-statements
        self.maxDiff = None  # pylint: disable=invalid-name
        self.max_pipeline_id = 0
        self.max_buildrun_id = 0
        self.max_testrun_id = 0

        def create_get_next_id():
            """Make a function returning a sequence of IDs."""
            next_id = 0

            def get_next_id():
                nonlocal next_id
                return_id = next_id
                next_id = next_id + 1
                return return_id

            return get_next_id

        get_next_pipeline_id = create_get_next_id()
        get_next_jid = create_get_next_id()
        get_next_recipe_id = create_get_next_id()
        get_next_task_id = create_get_next_id()

        # Common objects
        project = models.Project.objects.create(project_id=0)
        stage_merge = models.Stage.objects.create(name="merge")
        stage_build = models.Stage.objects.create(name="build")
        stage_test = models.Stage.objects.create(name="test")
        arch_x86_64 = models.Architecture.objects.create(name="x86_64")
        arch_aarch64 = models.Architecture.objects.create(name="aarch64")
        arch_ppc64 = models.Architecture.objects.create(name="ppc64")
        beaker_resource1 = \
            models.BeakerResource.objects.create(fqdn="host1.example.com")
        beaker_resource2 = \
            models.BeakerResource.objects.create(fqdn="host2.example.com")
        result_pass = models.TestResult.objects.create(name="PASS")
        result_fail = models.TestResult.objects.create(name="FAIL")
        testmaintainer_john_dee = \
            models.TestMaintainer.objects.create(name="John Dee",
                                                 email="john@gov.uk")
        testmaintainer_barbra_streisand = \
            models.TestMaintainer.objects.create(name="Barbra Streisand",
                                                 email="barbra@hollywood.com")
        test_a = models.Test.objects.create(name="Test A")
        test_a.maintainers.add(testmaintainer_john_dee)
        test_b = models.Test.objects.create(name="Test B", universal_id="test_b")
        test_b.maintainers.add(testmaintainer_barbra_streisand)
        test_c = models.Test.objects.create(name="Test C")
        test_c.maintainers.add(testmaintainer_john_dee)
        test_c.maintainers.add(testmaintainer_barbra_streisand)

        gittree = models.GitTree.objects.create(name='upstream-stable')

        # Pipeline without patches
        pipeline_without_patches = \
            models.Pipeline.objects.create(pipeline_id=get_next_pipeline_id(),
                                           project=project, gittree=gittree,
                                           created_at=ttd('2019-11-14 00:00'))
        self.max_pipeline_id = pipeline_without_patches.id
        create_pipeline_trigger_variables(
            pipeline_without_patches,
            git_url="https://git.kernel.org/torvalds/linux.git",
            commit_hash="393771b8e43a3eb4df1d759508f7b74f638eeb50",
            branch="master",
        )

        # Pipeline with patches, with successful merge
        pipeline_with_patches_merged = \
            models.Pipeline.objects.create(pipeline_id=get_next_pipeline_id(),
                                           project=project, gittree=gittree,
                                           started_at=ttd('2019-11-14 00:07'),
                                           created_at=ttd('2019-11-14 00:00'))
        self.max_pipeline_id = pipeline_with_patches_merged.id
        create_pipeline_trigger_variables(
            pipeline_with_patches_merged,
            git_url="https://git.kernel.org/torvalds/linux.git",
            commit_hash="ae26c7278f1f09d8202b7217b942ccd9815c5d79",
            branch="master",
            patch_urls="https://example.com/p1.mbox "
                       "https://example.com/p2.mbox",
        )
        add_patches(pipeline_with_patches_merged)
        models.MergeRun.objects.create(
            pipeline=pipeline_with_patches_merged,
            job=models.Job.objects.create(name="merge", stage=stage_merge),
            jid=get_next_jid(),
            success=True
        )

        # Pipeline with patches, with failed merge
        pipeline_with_patches_not_merged = \
            models.Pipeline.objects.create(pipeline_id=get_next_pipeline_id(),
                                           project=project, gittree=gittree,
                                           started_at=ttd('2019-11-14 00:07'),
                                           created_at=ttd('2019-11-14 00:00'))
        self.max_pipeline_id = pipeline_with_patches_not_merged.id
        create_pipeline_trigger_variables(
            pipeline_with_patches_not_merged,
            git_url="https://git.kernel.org/torvalds/linux.git",
            commit_hash="3eef6c7eeb3ad3638cb6d859b48a564e01c12ca6",
            branch="master",
            patch_urls="https://example.com/p1.mbox "
                       "https://example.com/p2.mbox",
        )
        add_patches(pipeline_with_patches_not_merged)
        models.MergeRun.objects.create(
            pipeline=pipeline_with_patches_not_merged,
            job=models.Job.objects.create(name="merge", stage=stage_merge),
            jid=get_next_jid(),
            success=False
        )

        # Pipeline with three builds, two successful, one failed, and tests
        pipeline_with_three_builds = \
            models.Pipeline.objects.create(pipeline_id=get_next_pipeline_id(),
                                           project=project, gittree=gittree,
                                           started_at=ttd('2019-11-14 00:07'),
                                           created_at=ttd('2019-11-14 00:00'))
        self.max_pipeline_id = pipeline_with_three_builds.id
        create_pipeline_trigger_variables(
            pipeline_with_three_builds,
            git_url="https://git.kernel.org/torvalds/linux.git",
            commit_hash="f95c24cc76be04acac65e38cb4810cb4f6f409b7",
            branch="master",
        )
        models.MergeRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=models.Job.objects.create(name="merge", stage=stage_merge),
            jid=get_next_jid(),
            success=True
        )
        # Builds
        self.max_buildrun_id = models.BuildRun.objects.create(
            pipeline=pipeline_with_three_builds,
            kernel_arch=arch_x86_64,
            make_opts="bzImage",
            kernel_package_url="https://example.com/kernel.tar.bz2",
            job=models.Job.objects.create(name="build x86_64",
                                          stage=stage_build),
            jid=get_next_jid(),
            success=True,
            duration=321,
            compiler=models.Compiler.objects.create(name="foo 1.2")
        ).id
        self.max_buildrun_id = models.BuildRun.objects.create(
            pipeline=pipeline_with_three_builds,
            kernel_arch=arch_ppc64,
            make_opts="bzImage",
            kernel_package_url="https://example.com/kernel.tar.bz2",
            job=models.Job.objects.create(name="build ppc64",
                                          stage=stage_build),
            jid=get_next_jid(),
            success=True,
            duration=None
        ).id
        self.max_buildrun_id = models.BuildRun.objects.create(
            pipeline=pipeline_with_three_builds,
            kernel_arch=arch_aarch64,
            make_opts="bzImage",
            kernel_package_url="https://example.com/kernel.tar.bz2",
            job=models.Job.objects.create(name="build aarch64",
                                          stage=stage_build),
            jid=get_next_jid(),
            success=False
        ).id

        # Tests for first build
        job = models.Job.objects.create(name="test x86_64", stage=stage_test)
        jid = get_next_jid()
        recipe_id = get_next_recipe_id()
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_x86_64,
            beaker_resource=beaker_resource1,
            task_id=get_next_task_id(),
            test=test_a,
            result=result_pass,
            retcode=0,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
            duration=123,
        ).id
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_x86_64,
            beaker_resource=beaker_resource1,
            task_id=get_next_task_id(),
            test=test_b,
            result=result_pass,
            retcode=0,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
            duration=123,
        ).id
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_x86_64,
            beaker_resource=beaker_resource1,
            task_id=get_next_task_id(),
            test=test_c,
            result=result_pass,
            retcode=0,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
            duration=123,
        ).id

        # Tests for second build
        job = models.Job.objects.create(name="test ppc64", stage=stage_test)
        jid = get_next_jid()
        recipe_id = get_next_recipe_id()
        # Passed, non-waived
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_ppc64,
            beaker_resource=beaker_resource2,
            task_id=get_next_task_id(),
            test=test_a,
            result=result_pass,
            retcode=0,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
            duration=123,
        ).id
        # Failed, waived, no started_at
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_ppc64,
            beaker_resource=beaker_resource2,
            task_id=get_next_task_id(),
            test=test_b,
            result=result_fail,
            retcode=1,
            waived=True,
            duration=123,
        ).id
        # Failed, non-waived, no duration
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_ppc64,
            beaker_resource=beaker_resource2,
            task_id=get_next_task_id(),
            test=test_c,
            result=result_fail,
            retcode=1,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
        ).id

        artifacts = {}
        for name in ['testfile.log', 'kernel.tar.gz', 'kernel.config']:
            artifacts[name] = models.Artifact.objects.create(
                name=name,
                url=f'http://s3.server/{name}'
            )

        models.BuildRun.objects.filter(pipeline=pipeline_with_three_builds)\
              .first().artifacts.add(artifacts['kernel.tar.gz'], artifacts['kernel.config'])
        models.BeakerTestRun.objects.filter(pipeline=pipeline_with_three_builds)\
              .last().logs.add(artifacts['testfile.log'])
        models.BuildRun.objects.filter(pipeline=pipeline_with_three_builds).update(log=artifacts['testfile.log'])
        models.MergeRun.objects.filter(pipeline=pipeline_with_three_builds).update(log=artifacts['testfile.log'])

        self.expected_result = (
            {
                "version": {"major": 3, "minor": 0},
                "revisions": [
                    {
                        "git_repository_branch": "master",
                        "git_repository_commit_hash": "393771b8e43a3eb4df1d759508f7b74f638eeb50",
                        "git_repository_url": "https://git.kernel.org/torvalds/linux.git",
                        "misc": {"pipeline_id": 0},
                        "origin": "redhat",
                        "origin_id":
                            "393771b8e43a3eb4df1d759508f7b74f638eeb50",
                        "patch_mboxes": [],
                        "valid": True,
                        "contacts": [],
                        'discovery_time': '2019-11-14T00:00:00Z',
                    },
                    {
                        "git_repository_branch": "master",
                        "git_repository_commit_hash": "3eef6c7eeb3ad3638cb6d859b48a564e01c12ca6",
                        "git_repository_url": "https://git.kernel.org/torvalds/linux.git",
                        "misc": {"pipeline_id": 2},
                        "origin": "redhat",
                        "origin_id":
                            "3eef6c7eeb3ad3638cb6d859b48a564e01c12ca6"
                            "+9e1a8dc4c6a0d43442f4991ee9fdbc5453e1c0a8341bbdb817393278ff00aa83",
                        "patch_mboxes": [
                            {"name": "p1.mbox", "url": "https://example.com/p1.mbox"},
                            {"name": "p2.mbox", "url": "https://example.com/p2.mbox"},
                        ],
                        "valid": False,
                        "contacts": [],
                        'discovery_time': '2019-11-14T00:00:00Z',
                    },
                    {
                        "git_repository_branch": "master",
                        "git_repository_commit_hash": "ae26c7278f1f09d8202b7217b942ccd9815c5d79",
                        "git_repository_url": "https://git.kernel.org/torvalds/linux.git",
                        "misc": {"pipeline_id": 1},
                        "origin": "redhat",
                        "origin_id":
                            "ae26c7278f1f09d8202b7217b942ccd9815c5d79"
                            "+9e1a8dc4c6a0d43442f4991ee9fdbc5453e1c0a8341bbdb817393278ff00aa83",
                        "patch_mboxes": [
                            {"name": "p1.mbox", "url": "https://example.com/p1.mbox"},
                            {"name": "p2.mbox", "url": "https://example.com/p2.mbox"},
                        ],
                        "valid": True,
                        "contacts": [],
                        'discovery_time': '2019-11-14T00:00:00Z',
                    },
                    {
                        "git_repository_branch": "master",
                        "git_repository_commit_hash": "f95c24cc76be04acac65e38cb4810cb4f6f409b7",
                        "git_repository_url": "https://git.kernel.org/torvalds/linux.git",
                        "misc": {"pipeline_id": 3},
                        "origin": "redhat",
                        "origin_id":
                            "f95c24cc76be04acac65e38cb4810cb4f6f409b7",
                        "patch_mboxes": [],
                        "valid": True,
                        "contacts": [],
                        'discovery_time': '2019-11-14T00:00:00Z',
                        "log_url": "http://s3.server/testfile.log",
                    },
                ],
                "builds": [
                    {
                        'architecture': 'x86_64',
                        'command': 'make bzImage',
                        'compiler': 'foo 1.2',
                        'duration': 321,
                        'misc': {'job_id': 3, 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '3',
                        'revision_origin': 'redhat',
                        'revision_origin_id':
                            'f95c24cc76be04acac65e38cb4810cb4f6f409b7',
                        'valid': True,
                        'config_name': 'fedora',
                        'config_url': 'http://s3.server/kernel.config',
                        'output_files': [{'name': 'kernel.tar.gz', 'url': 'http://s3.server/kernel.tar.gz'}],
                        'start_time': '2019-11-14T00:07:00Z',
                        'log_url': 'http://s3.server/testfile.log',
                    },
                    {
                        'architecture': 'ppc64',
                        'command': 'make bzImage',
                        'misc': {'job_id': 4, 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '4',
                        'revision_origin': 'redhat',
                        'revision_origin_id':
                            'f95c24cc76be04acac65e38cb4810cb4f6f409b7',
                        'valid': True,
                        'config_name': 'fedora',
                        'output_files': [],
                        'start_time': '2019-11-14T00:07:00Z',
                        'log_url': 'http://s3.server/testfile.log',
                    },
                    {
                        'architecture': 'aarch64',
                        'command': 'make bzImage',
                        'misc': {'job_id': 5, 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '5',
                        'revision_origin': 'redhat',
                        'revision_origin_id':
                            'f95c24cc76be04acac65e38cb4810cb4f6f409b7',
                        'valid': False,
                        'config_name': 'fedora',
                        'output_files': [],
                        'start_time': '2019-11-14T00:07:00Z',
                        'log_url': 'http://s3.server/testfile.log',
                    }
                ],
                "tests": [
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '3',
                        'description': 'Test A',
                        'misc': {'beaker_recipe_id': 0,
                                 'beaker_task_id': 0,
                                 'job_id': 6,
                                 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '0',
                        'status': 'PASS',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'duration': 123,
                        'output_files': [],
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '3',
                        'description': 'Test B',
                        'misc': {'beaker_recipe_id': 0,
                                 'beaker_task_id': 1,
                                 'job_id': 6,
                                 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '1',
                        'status': 'PASS',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'duration': 123,
                        'output_files': [],
                        'path': "test_b",
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '3',
                        'description': 'Test C',
                        'misc': {'beaker_recipe_id': 0,
                                 'beaker_task_id': 2,
                                 'job_id': 6,
                                 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '2',
                        'status': 'PASS',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'duration': 123,
                        'output_files': [],
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '4',
                        'description': 'Test A',
                        'misc': {'beaker_recipe_id': 1,
                                 'beaker_task_id': 3,
                                 'job_id': 7,
                                 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '3',
                        'status': 'PASS',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'duration': 123,
                        'output_files': [],
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '4',
                        'description': 'Test B',
                        'misc': {'beaker_recipe_id': 1,
                                 'beaker_task_id': 4,
                                 'job_id': 7,
                                 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '4',
                        'status': 'FAIL',
                        'waived': True,
                        'duration': 123,
                        'output_files': [],
                        'path': "test_b",
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '4',
                        'description': 'Test C',
                        'misc': {'beaker_recipe_id': 1,
                                 'beaker_task_id': 5,
                                 'job_id': 7,
                                 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '5',
                        'status': 'FAIL',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'output_files': [{'name': 'testfile.log', 'url': 'http://s3.server/testfile.log'}],
                    }
                ]
            },
            self.max_pipeline_id,
            self.max_buildrun_id,
            self.max_testrun_id,
        )

    def test_revisions(self):
        """Test the revision endpoint."""
        response = self.client.get('/kcidb/1/data/revisions?last_retrieved_id=-1')
        data = response.json()['results']['data']

        self.expected_result[0]['revisions'].sort(key=lambda x: x['misc']["pipeline_id"])

        self.assertListEqual(
            self.expected_result[0]['revisions'],
            data['revisions']
        )

        kcidb.io_schema.validate(data)

    def test_builds(self):
        """Test the builds endpoint."""
        response = self.client.get('/kcidb/1/data/builds?last_retrieved_id=-1')
        data = response.json()['results']['data']

        self.expected_result[0]['builds'].sort(key=lambda x: x["origin_id"])

        self.assertListEqual(
            self.expected_result[0]['builds'],
            data['builds']
        )

        kcidb.io_schema.validate(data)

    def test_tests(self):
        """Test the tests endpoint."""
        response = self.client.get('/kcidb/1/data/tests?last_retrieved_id=-1')
        data = response.json()['results']['data']

        self.expected_result[0]['tests'].sort(key=lambda x: x["origin_id"])

        self.assertListEqual(
            self.expected_result[0]['tests'],
            data['tests']
        )

        kcidb.io_schema.validate(data)

    def test_pagination(self):
        """Test the endpoint pagination."""

        # a: [0] 1 2
        response_a = self.client.get('/kcidb/1/data/tests?last_retrieved_id=-1&limit=1&offset=0')
        self.assertEqual(1, len(response_a.json()['results']['data']['tests']))

        # b: [0 1] 2
        response_b = self.client.get('/kcidb/1/data/tests?last_retrieved_id=-1&limit=2&offset=0')
        self.assertEqual(2, len(response_b.json()['results']['data']['tests']))

        # c: 0 [1 2]
        response_c = self.client.get('/kcidb/1/data/tests?last_retrieved_id=-1&limit=2&offset=1')
        self.assertEqual(2, len(response_c.json()['results']['data']['tests']))

        # a.0 == b.0
        self.assertEqual(
            response_a.json()['results']['data']['tests'][0],
            response_b.json()['results']['data']['tests'][0],
            )

        # b.1 == c.0
        self.assertEqual(
            response_b.json()['results']['data']['tests'][1],
            response_c.json()['results']['data']['tests'][0],
            )

    def test_date_filter(self):
        """Test the endpoint min_pipeline_started_at filter."""

        dates = [
            ttd("2000-02-02 04:00:00"),
            ttd("2000-02-02 03:00:00"),
            ttd("2000-02-02 02:00:00"),
            ttd("2000-02-02 01:00:00"),
        ]

        for index, pipeline in enumerate(models.Pipeline.objects.all()):
            pipeline.started_at = dates[index]
            pipeline.save()

        # No time filter
        response = self.client.get('/kcidb/1/data/revisions?last_retrieved_id=-1&limit=3')
        self.assertEqual(3, len(response.json()['results']['data']['revisions']))

        # Old date. Should return all 4.
        response = self.client.get('/kcidb/1/data/revisions?last_retrieved_id=-1&'
                                   'min_pipeline_started_at=2000-02-01 02:00')
        self.assertEqual(4, len(response.json()['results']['data']['revisions']))

        # Date between the list. Should return the latest 2.
        response = self.client.get('/kcidb/1/data/revisions?last_retrieved_id=-1&'
                                   'min_pipeline_started_at=2000-02-02 03:00')
        self.assertEqual(2, len(response.json()['results']['data']['revisions']))
        self.assertEqual(
            models.Pipeline.objects.get(started_at=dates[1]).pipeline_id,
            response.json()['results']['data']['revisions'][0]['misc']['pipeline_id']
        )
        self.assertEqual(
            models.Pipeline.objects.get(started_at=dates[0]).pipeline_id,
            response.json()['results']['data']['revisions'][1]['misc']['pipeline_id']
        )

    def test_last_retrived_id(self):
        """Test last_retrieved_id is ok."""
        # Returned the first item.
        response = self.client.get('/kcidb/1/data/revisions?last_retrieved_id=-1&limit=1')
        self.assertEqual(
            models.Pipeline.objects.last().id,
            response.json()['results']['last_retrieved_id'])

        # Nothing newer than 123456 found. Returning 123456.
        response = self.client.get('/kcidb/1/data/revisions?last_retrieved_id=123456&limit=1')
        self.assertEqual(123456, response.json()['results']['last_retrieved_id'])


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestSubmit(utils.TestCase):
    """Test submit endpoint."""

    def test_schema_version(self):
        """Test schema version is validated."""
        data = {'version': {'major': 1, 'minor': 0}}
        self.assert_authenticated_post(
            400, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

        data = {'version': {'major': 3, 'minor': 0}}
        self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def test_full_submit(self):
        """Test submitting all the data."""
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {'origin': 'redhat', 'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'},
            ],
            'builds': [
                {'origin': 'redhat', 'revision_id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1'},
                {'origin': 'redhat', 'revision_id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2'},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1'},
                {'origin': 'redhat', 'build_id': 'redhat:build-2', 'id': 'redhat:test-2'},
            ]
        }

        self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

        for revision in data['revisions']:
            self.assertTrue(models.KCIDBRevision.objects.filter(id=revision['id']).exists())

        for build in data['builds']:
            self.assertTrue(models.KCIDBBuild.objects.filter(id=build['id']).exists())

        for test in data['tests']:
            self.assertTrue(models.KCIDBTest.objects.filter(id=test['id']).exists())

    def test_errors(self):
        """
        Test submitting data with errors.

        It should submit all it can and return the errors.
        """
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {'origin': 'redhat', 'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'},
                {'origin': 'redhat', 'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'},
            ]
        }

        self.assertFalse(
            models.KCIDBRevision.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )
        response = self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )
        self.assertTrue(
            models.KCIDBRevision.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )
        self.assertDictEqual(
            {
                'errors': {
                    'revisions': {
                        'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3':
                        'Entry with redhat https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3 already exists.'
                    }
                }
            },
            response.json()
        )

    def test_retrigger(self):
        """Test submitting retriggered pipeline."""
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {
                    'origin': 'redhat',
                    'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                    'misc': {'pipeline': {'variables': {'retrigger': 'true'}}}
                },
            ]
        }
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {
                    'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                    'origin': 'redhat',
                    'valid': True,
                    'misc': {
                        'job': {
                            'id': 887316,
                            'name': 'merge',
                            'stage': 'merge',
                            'started_at': '2020-06-03T15:11:19.327Z',
                            'created_at': '2020-06-03T15:08:05.512Z',
                            'finished_at': '2020-06-03T15:14:55.148Z',
                            'duration': 215.820987,
                            'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                            'tag': '-209.el8',
                            'commit_message_title': '[redhat] kernel',
                            'kernel_version': None
                        },
                        'pipeline': {
                            'id': 592705,
                            'variables': {
                                'cki_pipeline_type': 'patchwork',
                                'retrigger': 'true'
                            },
                            'started_at': '2020-06-03T15:08:09.957Z',
                            'created_at': '2020-06-03T15:08:05.288Z',
                            'finished_at': None,
                            'duration': None,
                            'ref': 'retrigger-67609c5a-d0c6-43ed-b445-2c5c985871b4',
                            'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                            'project': {
                                'id': 2,
                                'path_with_namespace': 'cki-project/cki-pipeline',
                            }
                        }
                    }
                }
            ]
        }

        # Submit the pipeline with ALLOW_DEBUG_PIPELINES=False
        with mock.patch('datawarehouse.api.kcidb.views.settings.ALLOW_DEBUG_PIPELINES', False):
            self.assert_authenticated_post(
                201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
                content_type="application/json"
            )

        self.assertFalse(
            models.KCIDBRevision.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )

        # Submit the pipeline with ALLOW_DEBUG_PIPELINES=True
        with mock.patch('datawarehouse.api.kcidb.views.settings.ALLOW_DEBUG_PIPELINES', True):
            self.assert_authenticated_post(
                201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
                content_type="application/json"
            )

        revision = models.KCIDBRevision.objects.get(
            id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
        )
        self.assertEqual(
            'retriggers',
            models.Pipeline.objects.get(merge_jobs__kcidb_revision=revision).gittree.name
        )

    @mock.patch('datawarehouse.models.Pipeline.objects.get')
    def test_notify_tests_jobs_finished(self, pipeline_get):
        # pylint: disable=no-self-use
        """Test notify_tests_jobs_finished."""
        def side_effect(pipeline_id):
            return pipeline_id
        pipeline_get.side_effect = side_effect

        data = {
            'builds': [
                {'id': 1, 'misc': {'pipeline': {'id': 10}}},
            ],
            'tests': [
                {'id': 1, 'misc': {}},
                {'id': 2, 'misc': {'pipeline': {'id': 11}}},
                {'id': 3, 'misc': {'pipeline': {'id': 11}}},
                {'id': 4, 'misc': {'pipeline': {'id': 12}}},
                {'id': 5, 'misc': {'pipeline': {'id': 13}}},
                {'id': 6, 'misc': {'pipeline': {'id': 14}}},
            ]
        }
        errors = {'tests': {6: 'Not submitted because of ble ble'}}

        with mock.patch('datawarehouse.signals.pipeline_processing_done.send') as send_mock:
            views.Submit.notify_tests_jobs_finished(data, errors)
            send_mock.assert_has_calls(
                [
                    mock.call(pipeline=11, sender='api.kcidb.submit'),
                    mock.call(pipeline=12, sender='api.kcidb.submit'),
                    mock.call(pipeline=13, sender='api.kcidb.submit'),
                ],
                any_order=True
            )

    def test_notify_tests_jobs_finished_is_called(self):
        """Test the method notify_tests_jobs_finished is called when info is submitted."""
        data = {
            'version': {'major': 3, 'minor': 0},
        }

        with mock.patch('datawarehouse.api.kcidb.views.Submit.notify_tests_jobs_finished') as notify_mock:
            self.assert_authenticated_post(
                201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
                content_type="application/json"
            )
            self.assertTrue(notify_mock.called)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestEndpoints(utils.TestCase):
    """Test kcidb get/list endpoints."""

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set up data."""
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {'origin': 'redhat', 'id': 'decd6167bf4f6bec1284006d0522381b44660df3'},
            ],
            'builds': [
                {'origin': 'redhat', 'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1'},
                {'origin': 'redhat', 'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2'},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1'},
                {'origin': 'redhat', 'build_id': 'redhat:build-2', 'id': 'redhat:test-2'},
            ]
        }

        self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def test_revisions_list(self):
        """Test revisions list endpoint."""
        revision = models.KCIDBRevision.objects.first()
        response = self.client.get('/api/1/kcidb/revisions')
        self.assertEqual(
            serializers.KCIDBRevisionSerializer([revision], many=True).data,
            response.json()['results']
        )

    def test_revisions_get(self):
        """Test revisions get endpoint. Both iid and id queries."""
        revision = models.KCIDBRevision.objects.first()
        response_iid = self.client.get(f'/api/1/kcidb/revisions/{revision.iid}')
        response_id = self.client.get(f'/api/1/kcidb/revisions/{revision.id}')
        self.assertEqual(
            response_id.json(),
            response_iid.json()
        )
        self.assertEqual(
            serializers.KCIDBRevisionSerializer(revision).data,
            response_id.json()
        )

    def test_builds_list(self):
        """Test builds list endpoint."""
        revision = models.KCIDBRevision.objects.first()
        response = self.client.get(f'/api/1/kcidb/revisions/{revision.iid}/builds')
        self.assertEqual(
            serializers.KCIDBBuildSerializer(revision.kcidbbuild_set.all(), many=True).data,
            response.json()['results']
        )

    def test_builds_get(self):
        """Test builds get endpoint. Both iid and id queries."""
        revision = models.KCIDBRevision.objects.first()
        build = revision.kcidbbuild_set.first()
        response_iid = self.client.get(f'/api/1/kcidb/builds/{build.iid}')
        response_id = self.client.get(f'/api/1/kcidb/builds/{build.id}')
        self.assertEqual(
            response_id.json(),
            response_iid.json()
        )
        self.assertEqual(
            serializers.KCIDBBuildSerializer(build).data,
            response_id.json()
        )

    def test_tests_list(self):
        """Test builds list endpoint."""
        revision = models.KCIDBRevision.objects.first()
        build = revision.kcidbbuild_set.first()
        response = self.client.get(f'/api/1/kcidb/builds/{build.id}/tests')
        self.assertEqual(
            serializers.KCIDBTestSerializer(build.kcidbtest_set.all(), many=True).data,
            response.json()['results']
        )

    def test_tests_get(self):
        """Test tests get endpoint. Both iid and id queries."""
        revision = models.KCIDBRevision.objects.first()
        build = revision.kcidbbuild_set.first()
        test = build.kcidbtest_set.first()
        response_iid = self.client.get(f'/api/1/kcidb/tests/{test.iid}')
        response_id = self.client.get(f'/api/1/kcidb/tests/{test.id}')
        self.assertEqual(
            response_id.json(),
            response_iid.json()
        )
        self.assertEqual(
            serializers.KCIDBTestSerializer(test).data,
            response_id.json()
        )
