from django.db import migrations

class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0094_set_skipped_on_invalid'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='testrun',
            name='invalid_result',
        ),
        migrations.RemoveField(
            model_name='testrun',
            name='untrusted',
        ),
    ]
