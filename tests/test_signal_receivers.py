"""Test the signal_receivers module."""
from unittest import mock
from freezegun import freeze_time

from django import test

from datawarehouse import models, signals, signal_receivers
from datawarehouse.api.kcidb import serializers as kcidb_serializers


class SignalsTest(test.TestCase):
    """Unit tests for the Signals module."""

    @staticmethod
    def _create_pipeline():
        """Create and return pipeline."""
        project, _ = models.Project.objects.get_or_create(project_id=0)
        gittree, _ = models.GitTree.objects.get_or_create(name='Tree 1')
        pipeline_id = models.Pipeline.objects.count() + 1
        pipeline = models.Pipeline.objects.create(
            pipeline_id=pipeline_id,
            project=project,
            gittree=gittree,
        )
        return pipeline

    @staticmethod
    def _create_action(name, pipeline):
        """Create and return action."""
        kind, _ = models.ActionKind.objects.get_or_create(name=name)
        signal = models.Action.objects.create(kind=kind, pipeline=pipeline)
        return signal

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_pipeline_notification')
    def test_send_pipeline_message_running(self, send_pipeline_notification):
        """Test that the pipeline message is sent correctly. Status running."""
        pipeline = self._create_pipeline()
        signal_receivers.send_pipeline_message(pipeline=pipeline)

        send_pipeline_notification.assert_called_with({
            'timestamp': '2010-01-02T09:00:00+00:00',
            'status': 'running',
            'pipeline_id': pipeline.pipeline_id,
        })

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_pipeline_notification')
    def test_send_pipeline_message_finished(self, send_pipeline_notification):
        """Test that the pipeline message is sent correctly. Status finished."""
        pipeline = self._create_pipeline()
        pipeline.finished_at = '2010-01-02T09:00:00+00:00'
        signal_receivers.send_pipeline_message(pipeline=pipeline)

        send_pipeline_notification.assert_called_with({
            'timestamp': '2010-01-02T09:00:00+00:00',
            'status': 'finished',
            'pipeline_id': pipeline.pipeline_id,
        })

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_pipeline_notification')
    def test_ready_to_report_message(self, send_pipeline_notification):
        """Test that the report message is sent correctly."""
        pipeline = self._create_pipeline()
        action = self._create_action('triaged', pipeline)
        signal_receivers.send_ready_to_report_message(action=action)

        send_pipeline_notification.assert_called_with(
            {
                'timestamp': '2010-01-02T09:00:00+00:00',
                'status': 'ready_to_report',
                'pipeline_id': pipeline.pipeline_id,
            }
        )

    @mock.patch('datawarehouse.signal_receivers.utils.send_pipeline_notification')
    def test_ready_to_report_message_multiple_triage(self, send_pipeline_notification):
        """Test that the report message is sent only on the first triage."""
        pipeline = self._create_pipeline()
        action = self._create_action('triaged', pipeline)
        signal_receivers.send_ready_to_report_message(action=action)

        self.assertEqual(1, send_pipeline_notification.call_count)

        # Another triage action doesn't send message.
        action = self._create_action('triaged', pipeline)
        signal_receivers.send_ready_to_report_message(action=action)

        self.assertEqual(1, send_pipeline_notification.call_count)

    @mock.patch('datawarehouse.signal_receivers.utils.send_pipeline_notification')
    def test_ready_to_report_message_wrong_action(self, send_pipeline_notification):
        """Test that the report message is sent only with triage action."""
        pipeline = self._create_pipeline()
        action = self._create_action('something', pipeline)
        signal_receivers.send_ready_to_report_message(action=action)

        self.assertEqual(0, send_pipeline_notification.call_count)

    @mock.patch('datawarehouse.signal_receivers.utils.send_pipeline_notification')
    def test_ready_to_report_message_wrong_action_previously_triaged(self, send_pipeline_notification):
        """Test that the report message is sent only with triage action even if it was traiged before."""
        pipeline = self._create_pipeline()
        self._create_action('triaged', pipeline)
        action = self._create_action('something', pipeline)
        signal_receivers.send_ready_to_report_message(action=action)

        self.assertEqual(0, send_pipeline_notification.call_count)

    @staticmethod
    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification')
    def test_send_kcidb_message(send_kcidb):
        """Test that the kcidb message is sent correctly."""
        rev = models.KCIDBRevision.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
        )
        signal_receivers.send_kcidb_object_message(
            status='some-status',
            object_type='revision',
            object_instance=rev,
        )
        send_kcidb.assert_called_with({
            'timestamp': '2010-01-02T09:00:00+00:00',
            'status': 'some-status',
            'object_type': 'revision',
            'object': kcidb_serializers.KCIDBRevisionSerializer(rev).data,
            'id': rev.id,
            'iid': rev.iid,
        })

    @mock.patch('datawarehouse.signal_receivers.utils.send_pipeline_notification', mock.Mock())
    def test_signal_pipeline_processing_done(self):
        """Test pipeline_processing_done signal receivers."""
        pipeline = self._create_pipeline()
        receivers = signals.pipeline_processing_done.send(sender='test', pipeline=pipeline)
        self.assertEqual(
            [(signal_receivers.send_pipeline_message, None)],
            receivers
        )

    @mock.patch('datawarehouse.signal_receivers.utils.send_pipeline_notification', mock.Mock())
    def test_signal_action_created(self):
        """Test action_created signal receivers."""
        pipeline = self._create_pipeline()
        action = models.Action.objects.create(
            kind=models.ActionKind.objects.get_or_create(name='test')[0],
            pipeline=pipeline
        )
        receivers = signals.action_created.send(sender='test', action=action)
        self.assertEqual(
            [(signal_receivers.send_ready_to_report_message, None)],
            receivers
        )

    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification', mock.Mock())
    def test_signal_kcidb_object(self):
        """Test kcidb_object signal receivers."""
        rev = models.KCIDBRevision.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
        )
        receivers = signals.kcidb_object.send(
            sender='test', status='test', object_type='revision', object_instance=rev
        )
        self.assertEqual(
            [(signal_receivers.send_kcidb_object_message, None)],
            receivers
        )
