"""Test the views module."""
from datawarehouse import models
from tests import utils


class TestViewsInKCIDB(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Test context data in kcidb views."""

    def setUp(self):
        """Set up data."""
        origin = models.KCIDBOrigin.objects.create(name='redhat')

        self.revision = models.KCIDBRevision.objects.create(
            origin=origin, id='decd6167bf4f6bec1284006d0522381b44660df3'
        )

        self.revision_2 = models.KCIDBRevision.objects.create(
            origin=origin, id='decd6167bf4f6bec1284006d0522381b44660df4',
            valid=False,
        )

        self.revision_3 = models.KCIDBRevision.objects.create(
            origin=origin, id='decd6167bf4f6bec1284006d0522381b44660df5',
            valid=True,
        )

        self.build_1 = models.KCIDBBuild.objects.create(
            origin=origin, revision=self.revision, id='redhat:build-1'
        )
        self.build_2 = models.KCIDBBuild.objects.create(
            origin=origin, revision=self.revision, id='redhat:build-2',
            valid=False,
        )
        self.build_3 = models.KCIDBBuild.objects.create(
            origin=origin, revision=self.revision, id='redhat:build-3',
            valid=True,
        )
        self.test_1 = models.KCIDBTest.objects.create(
            origin=origin, build=self.build_1, id='redhat:test-1'
        )
        self.test_2 = models.KCIDBTest.objects.create(
            origin=origin, build=self.build_1, id='redhat:test-2',
            status=models.TestResult.objects.create(name='FAIL')
        )
        self.test_3 = models.KCIDBTest.objects.create(
            origin=origin, build=self.build_1, id='redhat:test-3',
            status=models.TestResult.objects.create(name='PASS')
        )

        issue_kind = models.IssueKind.objects.create(description='fail', tag='fail')

        self.issues = [
            models.Issue.objects.create(
                description=f'foo bar {i}', ticket_url=f'http://some.url.{i}', kind=issue_kind
            ) for i in range(5)
        ]

    def _assign_issues(self, issues):
        """Given a dict of object_name->list(issue_id), assign the issue to the object."""
        for object_name, issue_ids in issues.items():
            getattr(self, object_name).issues.add(
                *[self.issues[id] for id in issue_ids]
            )

    def test_revision_view(self):
        """Test revisions view."""
        self._assign_issues({
            'revision': [0],
            'build_1': [1, 2],
            'build_2': [1],
            'test_1': [3],
            'test_2': [3, 4],
        })

        response = self.client.get(f'/kcidb/revisions/{self.revision.iid}')
        self.assertContextEqual(
            response.context,
            {
                'issue_occurrences': [
                    {'issue': self.issues[4], 'revisions': [], 'builds': [], 'tests': [self.test_2]},
                    {'issue': self.issues[3], 'revisions': [], 'builds': [], 'tests': [self.test_1, self.test_2]},
                    {'issue': self.issues[2], 'revisions': [], 'builds': [self.build_1], 'tests': []},
                    {'issue': self.issues[1], 'revisions': [], 'builds': [self.build_2, self.build_1], 'tests': []},
                    {'issue': self.issues[0], 'revisions': [self.revision], 'builds': [], 'tests': []},
                ],
                'issues': models.Issue.objects.filter(resolved=False).order_by('-id'),
                'tests': [self.test_1, self.test_2, self.test_3],
                'tests_failed': [self.test_1, self.test_2],
                'builds': [self.build_3, self.build_2, self.build_1],
                'builds_failed': [self.build_2, self.build_1],
                'revision': self.revision,
                'revisions_failed': [self.revision],  # valid = None
            },
        )

        response = self.client.get(f'/kcidb/revisions/{self.revision_2.iid}')
        self.assertContextEqual(
            response.context,
            {
                'revisions_failed': [self.revision_2],  # valid = False
            },
        )

        response = self.client.get(f'/kcidb/revisions/{self.revision_3.iid}')
        self.assertContextEqual(
            response.context,
            {
                'revisions_failed': [],  # valid = True
            },
        )

    def test_revision_view_no_issues(self):
        """Test revisions view without issues."""
        response = self.client.get(f'/kcidb/revisions/{self.revision.iid}')
        self.assertEqual(response.context['issue_occurrences'], [])

    def test_build_view(self):
        """Test build view."""
        self._assign_issues({
            'build_1': [1, 2],
            'test_1': [3],
            'test_2': [3, 4],
        })

        response = self.client.get(f'/kcidb/builds/{self.build_1.iid}')
        self.assertContextEqual(
            response.context,
            {
                'issue_occurrences': [
                    {'issue': self.issues[4], 'builds': [], 'tests': [self.test_2]},
                    {'issue': self.issues[3], 'builds': [], 'tests': [self.test_1, self.test_2]},
                    {'issue': self.issues[2], 'builds': [self.build_1], 'tests': []},
                    {'issue': self.issues[1], 'builds': [self.build_1], 'tests': []},
                ],
                'issues': models.Issue.objects.filter(resolved=False).order_by('-id'),
                'tests': [self.test_1, self.test_2, self.test_3],
                'tests_failed': [self.test_1, self.test_2],
                'build': self.build_1,
                'builds_failed': [self.build_1],  # valid = None
            },
        )

        response = self.client.get(f'/kcidb/builds/{self.build_2.iid}')
        self.assertContextEqual(
            response.context,
            {
                'builds_failed': [self.build_2],  # valid = False
            },
        )

        response = self.client.get(f'/kcidb/builds/{self.build_3.iid}')
        self.assertContextEqual(
            response.context,
            {
                'builds_failed': [],  # valid = True
            },
        )

    def test_build_view_no_issues(self):
        """Test build view without issues."""
        response = self.client.get(f'/kcidb/builds/{self.build_1.iid}')
        self.assertEqual(response.context['issue_occurrences'], [])

    def test_test_view(self):
        """Test test view."""
        self._assign_issues({
            'test_1': [3, 4],
        })

        response = self.client.get(f'/kcidb/tests/{self.test_1.iid}')
        self.assertContextEqual(
            response.context,
            {
                'issue_occurrences': [
                    {'issue': self.issues[4], 'tests': [self.test_1]},
                    {'issue': self.issues[3], 'tests': [self.test_1]},
                ],
                'issues': models.Issue.objects.filter(resolved=False).order_by('-id'),
                'test': self.test_1,
                'tests_failed': [self.test_1],  # status = None
            },
        )

        response = self.client.get(f'/kcidb/tests/{self.test_2.iid}')
        self.assertContextEqual(
            response.context,
            {
                'tests_failed': [self.test_2],  # status = FAIL
            },
        )

        response = self.client.get(f'/kcidb/tests/{self.test_3.iid}')
        self.assertContextEqual(
            response.context,
            {
                'tests_failed': [],  # status = PASS
            },
        )

    def test_test_view_no_issues(self):
        """Test test view without issues."""
        response = self.client.get(f'/kcidb/tests/{self.test_1.iid}')
        self.assertEqual(response.context['issue_occurrences'], [])

    def test_create_issues_revision(self):
        """Test linking an issue to a revision."""
        self.assert_authenticated_post(
            302, 'change_kcidbrevision',
            '/kcidb/issues/occurrences',
            {
                'issue_id': self.issues[0].id,
                'revision_iids': [
                    self.revision.iid
                ],
            }
        )
        self.assertEqual(
            self.issues[0],
            self.revision.issues.get()
        )

    def test_create_issues_build(self):
        """Test linking an issue to some builds."""
        self.assert_authenticated_post(
            302, 'change_kcidbbuild',
            '/kcidb/issues/occurrences',
            {
                'issue_id': self.issues[0].id,
                'build_iids': [
                    self.build_1.iid,
                    self.build_2.iid,
                ]
            }
        )

        self.assertEqual(self.issues[0], self.build_1.issues.get())
        self.assertEqual(self.issues[0], self.build_2.issues.get())
        self.assertFalse(self.build_3.issues.exists())

    def test_create_issues_test(self):
        """Test linking an issue to some tests."""
        self.assert_authenticated_post(
            302, 'change_kcidbtest',
            '/kcidb/issues/occurrences',
            {
                'issue_id': self.issues[0].id,
                'test_iids': [
                    self.test_1.iid,
                    self.test_2.iid,
                ]
            }
        )

        self.assertEqual(self.issues[0], self.test_1.issues.get())
        self.assertEqual(self.issues[0], self.test_2.issues.get())
        self.assertFalse(self.test_3.issues.exists())

    def test_create_issues_all(self):
        """Test linking an issue to revisions, builds and tests."""
        self.assert_authenticated_post(
            302,
            [
                'change_kcidbrevision',
                'change_kcidbbuild',
                'change_kcidbtest',
            ],
            '/kcidb/issues/occurrences',
            {
                'issue_id': self.issues[1].id,
                'revision_iids': [
                    self.revision.iid,
                ],
                'build_iids': [
                    self.build_1.iid,
                    self.build_2.iid,
                ],
                'test_iids': [
                    self.test_1.iid,
                    self.test_2.iid,
                ],
            }
        )

        for elem in (self.revision, self.build_1, self.build_2, self.test_1, self.test_2):
            self.assertEqual(self.issues[1], elem.issues.get())

        for elem in (self.build_3, self.test_3):
            self.assertFalse(elem.issues.exists())

    def test_remove_issues_all(self):
        """Test unlinking an issue from revision, builds and tests."""
        self.revision.issues.add(self.issues[0])
        self.build_1.issues.add(self.issues[0])
        self.build_2.issues.add(self.issues[0])
        self.test_1.issues.add(self.issues[0])
        self.test_2.issues.add(self.issues[0])

        self.assert_authenticated_post(
            302,
            [
                'change_kcidbrevision',
                'change_kcidbbuild',
                'change_kcidbtest',
            ],
            '/kcidb/issues/occurrences',
            {
                'action': 'remove',
                'issue_id': self.issues[0].id,
                'revision_iids': [
                    self.revision.iid,
                ],
                'build_iids': [
                    self.build_1.iid,
                    self.build_2.iid,
                ],
                'test_iids': [
                    self.test_1.iid,
                    self.test_2.iid,
                ],
            }
        )

        for elem in (self.revision, self.build_1, self.build_2, self.test_1, self.test_2):
            self.assertFalse(elem.issues.exists())
