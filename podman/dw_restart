#!/bin/bash

CONTAINER_IMAGE_URL=registry.gitlab.com/cki-project/datawarehouse/datawarehouse

if [ -z $DB_PASSWORD ]; then
    echo "You need to specify the following environment variables:" 2>&1
    echo "  * POSTGRES_PASSWORD" 2>&1
    echo "  * GITLAB_URL" 2>&1
    echo "  * PATCHWORK_URL" 2>&1
    echo "  * BEAKER_URL" 2>&1
    echo "  * SECRET_KEY" 2>&1
    exit 1
fi

if ! podman container list | grep -q datawarehouse-db; then
    echo ">> Starting database container"
    podman start datawarehouse-db
fi

podman rm -f datawarehouse-web
echo ">> Refreshing datawarehouse-web container image"
podman pull $CONTAINER_IMAGE_URL

echo ">> Creating datawarehouse-web container"
podman create \
    --pod datawarehouse \
    --name datawarehouse-web \
    -e DB_HOST=${DB_HOST:-localhost} \
    -e DB_PASSWORD=$POSTGRES_PASSWORD \
    -e GITLAB_URL=$GITLAB_URL \
    -e PATCHWORK_URL=$PATCHWORK_URL \
    -e BEAKER_URL=$BEAKER_URL \
    -e SECRET_KEY=$SECRET_KEY \
    -e RABBITMQ_HOST=${RABBITMQ_HOST:-rabbitmq} \
    -e RABBITMQ_PORT=${RABBITMQ_PORT:-5672} \
    --ulimit nofile=90000:90000 \
    -v .:/code \
    $CONTAINER_IMAGE_URL

podman start datawarehouse-web

echo ">> Installing basic tools"
podman exec -it datawarehouse-web pip install tox pytest pytest-django
