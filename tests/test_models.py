"""Test the models module."""

from tests import utils

from datawarehouse import models


class ModelsTestCase(utils.TestCase):
    """Unit tests for the models module."""

    def setUp(self):
        """Set up tests."""
        self.insertObjects([
            {'model': 'datawarehouse.patch', 'fields': {'url': 'url 1', 'subject': 'subject 1'}},
            {'model': 'datawarehouse.patch', 'fields': {'url': 'url 2', 'subject': 'subject 2'}},
            {'model': 'datawarehouse.patchworkpatch', 'fields': {'patch_ptr': ['url 2', 'subject 2'],
                                                                 'web_url': 'web_url', 'patch_id': 1}},
        ])

    def test_patch_gui_url(self):
        """Test that the GUI URLs are correctly provided by the model."""
        self.assertEqual(models.PatchworkPatch.objects.first().gui_url, 'web_url')
        self.assertEqual(models.Patch.objects.first().gui_url, 'url 1')


class TestTestRun(utils.TestCase):
    """Test TestRun model."""

    def test_save(self):
        """Test save method populates skipped and passed."""
        project = models.Project.objects.create(project_id=0, path='cki-project/brew-pipeline')
        git_tree = models.GitTree.objects.create(name='Tree 1')
        pipeline = models.Pipeline.objects.create(pipeline_id=1, project=project, gittree=git_tree)
        test = models.Test.objects.create(id=1, name='test_1')
        kernel_arch = models.Architecture.objects.create(name='arch')

        fail_result = models.TestResult.objects.create(name='FAIL')
        pass_result = models.TestResult.objects.create(name='PASS')
        skip_result = models.TestResult.objects.create(name='SKIP')

        # Failed
        testrun = models.TestRun.objects.create(
            pipeline=pipeline, test=test, waived=False, kernel_arch=kernel_arch, result=fail_result
        )
        self.assertFalse(testrun.passed)
        self.assertFalse(testrun.skipped)

        # Skipped
        testrun.result = skip_result
        testrun.save()
        self.assertFalse(testrun.passed)
        self.assertTrue(testrun.skipped)

        # Passed
        testrun.result = pass_result
        testrun.save()
        self.assertTrue(testrun.passed)
        self.assertFalse(testrun.skipped)


class TestIssue(utils.TestCase):
    """Test issue model."""

    def setUp(self):
        """Set up data."""
        origin = models.KCIDBOrigin.objects.create(name='redhat')

        self.revision = models.KCIDBRevision.objects.create(
            origin=origin, id='decd6167bf4f6bec1284006d0522381b44660df3'
        )
        self.build_1 = models.KCIDBBuild.objects.create(
            origin=origin, revision=self.revision, id='redhat:build-1'
        )
        self.build_2 = models.KCIDBBuild.objects.create(
            origin=origin, revision=self.revision, id='redhat:build-2'
        )
        self.test_1 = models.KCIDBTest.objects.create(
            origin=origin, build=self.build_1, id='redhat:test-1'
        )
        self.test_2 = models.KCIDBTest.objects.create(
            origin=origin, build=self.build_1, id='redhat:test-2'
        )

        issue_kind = models.IssueKind.objects.create(description='fail', tag='fail')
        self.issue = models.Issue.objects.create(
            description='foo bar', ticket_url='http://some.url', kind=issue_kind
        )

    def test_revisions_empty(self):
        """Test revisions property."""
        self.assertEqual(0, self.issue.revisions.count())

    def test_revisions_direct(self):
        """Test revisions property."""
        self.revision.issues.set([self.issue])
        self.assertEqual(
            'decd6167bf4f6bec1284006d0522381b44660df3',
            self.issue.revisions.get().id,
        )

    def test_revisions_indirect_build(self):
        """Test revisions property."""
        self.build_1.issues.set([self.issue])
        self.assertEqual(
            'decd6167bf4f6bec1284006d0522381b44660df3',
            self.issue.revisions.get().id,
        )

        # Another build shouldn't change anything, it's already related.
        self.build_2.issues.set([self.issue])
        self.assertEqual(
            'decd6167bf4f6bec1284006d0522381b44660df3',
            self.issue.revisions.get().id,
        )

    def test_revisions_indirect_test(self):
        """Test revisions property."""
        self.test_1.issues.set([self.issue])
        self.assertEqual(
            'decd6167bf4f6bec1284006d0522381b44660df3',
            self.issue.revisions.get().id,
        )

        # Another test shouldn't change anything, it's already related.
        self.test_2.issues.set([self.issue])
        self.assertEqual(
            'decd6167bf4f6bec1284006d0522381b44660df3',
            self.issue.revisions.get().id,
        )

    def test_revisions_mixed(self):
        """Test revisions property."""
        self.revision.issues.set([self.issue])
        self.build_1.issues.set([self.issue])
        self.test_1.issues.set([self.issue])
        self.assertEqual(
            'decd6167bf4f6bec1284006d0522381b44660df3',
            self.issue.revisions.get().id,
        )
