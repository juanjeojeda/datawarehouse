# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Beaker helper."""
from functools import lru_cache

import requests

from cki.beaker_tools import utils as beaker_utils


class Beaker:
    # pylint: disable=too-few-public-methods
    """
    Beaker class.

    This should include all the methods defined on this file.. some day.
    """

    def __init__(self, url):
        """Init."""
        self.url = url

    @lru_cache(maxsize=128)
    def get_recipe(self, recipe_id):
        """Get recipe data."""
        url = f'{self.url}/recipes/{recipe_id}'
        response = requests.get(url, headers={'Accept': 'application/json'})
        return response.json()

    def get_task_result(self, task):
        """Get parsed task result."""
        # Need to hit recipe endpoint to get results data.
        recipe = self.get_recipe(task['recipe_id'])
        task = next(t for t in recipe['tasks'] if t['t_id'] == task['t_id'])

        # Extracted from cki-tools.cki.beaker_tools.process_logs.aggregate_from_beaker_xml
        skipped = all([
            result.get('path') == '/' and result.get('result') != 'Panic'
            for result in task['results']
        ])

        if task['result'] == 'New':
            # This is not handled by beaker_utils.decode_test_result.
            return 'NEW'

        return beaker_utils.decode_test_result(task['status'], task['result'], skipped)

    def get_recipeset_tasks(self, rset_id):
        """Download tasks data."""
        url = f"{self.url}/recipesets/{rset_id.split(':')[1]}"
        resp = requests.get(url)
        beaker_json = resp.json()

        raw_beaker_tasks = self.extract_tasks_from_recipeset(beaker_json)

        return self.parse_beaker_tasks(raw_beaker_tasks)

    @staticmethod
    def extract_tasks_from_recipeset(rset):
        """Parse Beaker recipeset JSON and return a list of tasks."""
        # Loop through each recipe and find the tasks in each.
        tasks = {}
        for recipe in rset['machine_recipes']:
            recipe_tasks = Beaker.filter_tasks_from_recipe(recipe)
            tasks[recipe["id"]] = {'task': recipe_tasks, 'resource': recipe['resource']}

        return tasks

    def parse_beaker_tasks(self, raw_beaker_tasks):
        """Parse beaker tasks from raw JSON and return simpler data."""
        parsed_beaker_tasks = {}
        for recipe_id, beaker_task_info in raw_beaker_tasks.items():
            beaker_tasks = beaker_task_info.get('task')
            beaker_resource = beaker_task_info.get('resource')
            parsed_beaker_tasks[recipe_id] = []
            for beaker_task in beaker_tasks:
                # Start with some basics for the task.
                result = self.get_task_result(beaker_task)
                temp_task = {
                    'name': beaker_task['name'],
                    'result': result,
                    'waived': False,
                    'maintainers': [],
                    'task_id': beaker_task['t_id'],
                    'beaker_resource_fqdn': beaker_resource['fqdn'] if beaker_resource else None,
                    'start_time': beaker_task['start_time'],
                    'finish_time': beaker_task['finish_time'],
                }

                # Is there a fetch URL?
                if 'fetch_url' in beaker_task:
                    temp_task['fetch_url'] = beaker_task['fetch_url']

                if 'params' in beaker_task:
                    # Is this task waived?
                    waived_params = Beaker._get_param_value(beaker_task['params'], 'CKI_WAIVED')
                    if waived_params:
                        temp_task['waived'] = True

                    # Get the test maintainers for this task.
                    maintainer_list = Beaker._get_param_value(beaker_task['params'], 'CKI_MAINTAINERS')

                    if maintainer_list:
                        temp_task['maintainers'] = maintainer_list.split(', ')

                    temp_task['cki_name'] = (
                        Beaker._get_param_value(beaker_task['params'], 'CKI_NAME') or
                        Beaker._get_param_value(beaker_task['params'], 'CKI_CASE_NAME')
                    )

                    temp_task['universal_id'] = Beaker._get_param_value(beaker_task['params'], 'CKI_UNIVERSAL_ID')

                parsed_beaker_tasks[recipe_id].append(temp_task)

        return parsed_beaker_tasks

    @staticmethod
    def filter_tasks_from_recipe(recipe):
        """Exclude tasks we don't care about."""
        tasks = []

        for task in recipe['tasks']:
            if any([param['name'] in ('CKI_NAME', 'CKI_CASE_NAME') for param in task['params']]):
                tasks.append(task)

        return tasks

    @staticmethod
    def _get_param_value(params_list, key_name):
        """
        Return a parameter value from a list of dicts.

        Given a list of dicts with keys 'name', and 'value', return value
        of certain element with name == key_name.

        [{'name': name, 'value': value}, ..]
        """
        for param in params_list:
            if param['name'] == key_name:
                return param['value']
        return None
