from django.db import migrations


def apply_skipped_on_results(apps, schema_editor):
    """Set skipped=True on SKIP results."""
    db_alias = schema_editor.connection.alias
    testrun_mgr = apps.get_model('datawarehouse', 'TestRun').objects.using(db_alias)
    testrun_mgr.filter(result__name='SKIP').update(skipped=True)


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0097_testrun_skipped'),
    ]

    operations = [
        migrations.RunPython(apply_skipped_on_results),
    ]
