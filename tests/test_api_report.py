"""Test the report api module."""
import os
import json
import datetime
from pathlib import Path
from pytz import UTC

from django.utils import timezone

from tests import utils
from datawarehouse import models, serializers


class APIReportTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the views module."""

    @staticmethod
    def load_email(email):
        """Load email from assets."""
        file_name = os.path.join(utils.ASSETS_DIR, email)
        file_content = Path(file_name).read_text()

        return {
            'content': json.loads(file_content)
        }

    def setUp(self):
        """Set up tests."""
        self.project = models.Project.objects.create(project_id=0)
        self.git_tree = models.GitTree.objects.create(name='Tree 1')

        cases = [
            # (pipeline_id, duration, created_at hours ago, finished_at hour ago),
            (1001, 123, 24, 23),
            (1002, 123, 20, 4),
            (1003, 123, 100, 23),
            (1004, 123, 2, 1),
        ]

        now = timezone.now()
        for pipeline_id, duration, ca_ha, fa_ha in cases:
            models.Pipeline.objects.create(pipeline_id=pipeline_id, project=self.project,
                                           gittree=self.git_tree, duration=duration,
                                           created_at=(now - datetime.timedelta(hours=ca_ha)),
                                           finished_at=(now - datetime.timedelta(hours=fa_ha)),
                                           )

    def test_report_create(self):
        """Test creating a report."""
        pipeline = models.Pipeline.objects.first()
        email = self.load_email('email_complete')

        self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")

        report = pipeline.reports.first()

        self.assertEqual(
            '\nHello,\n\nWe ran automated tests on a patchset that was proposed for merging into this\nkernel tree.',
            report.body)
        self.assertEqual(
            '=?utf-8?b?4pyF?= PASS: Re: [RHEL    PATCH 000000000 2/2] ' +
            'tpm: Revert\n\t"Lorem ipsum dolor sit amet, consectetur adipisci\'s"',
            report.subject)
        self.assertEqual(
            datetime.datetime(2020, 1, 9, 19, 33, 16, tzinfo=UTC),
            report.sent_at
        )
        self.assertEqual(
            '<cki.0.5FM2O05IQP@redhat.com>',
            report.msgid)

        report.addr_to.get(email='redacted-email-add@redhat.com')
        report.addr_to.get(email='redacted-email@redhat.com')
        report.addr_cc.get(email='test@redhat.com')

    def test_report_create_duplicated(self):
        """Test creating a report two times."""
        pipeline = models.Pipeline.objects.first()
        email = self.load_email('email_complete')

        # First time created.
        response = self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")
        self.assertEqual(1, pipeline.reports.count())

        self.assertEqual(
            serializers.ReportSerializer(pipeline.reports.first()).data,
            response.json()
        )

        # Second time not created.
        response = self.assert_authenticated_post(
            400, 'add_report',
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")
        self.assertEqual(b'"MsgID already exists."', response.content)
        self.assertEqual(1, pipeline.reports.count())

    def test_report_get(self):
        """Test getting pipeline's reports."""
        pipeline = models.Pipeline.objects.first()
        pipeline.reports.create(
            body='content',
            subject='subject',
            sent_at=timezone.now(),
            msgid='<someting@redhat.com>',
        )

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}/report')

        self.assertEqual(
            response.json()['results'],
            {
                'reports': serializers.ReportSerializer(pipeline.reports.all(), many=True).data,
            }
        )

    def test_report_create_emails_empty(self):
        """Test creating a report with empty email fields."""
        pipeline = models.Pipeline.objects.first()
        email = self.load_email('email_no_recipients')

        self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")

        report = pipeline.reports.first()

        self.assertEqual(0, report.addr_to.count())
        self.assertEqual(0, report.addr_cc.count())

    def test_create_encoded(self):
        """Test creating report with body encoded."""
        pipeline = models.Pipeline.objects.first()
        email = self.load_email('email_b64_encoded')

        self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")

        report = pipeline.reports.first()

        self.assertEqual(
            '\nHello,\n\nW',
            report.body[:10])

    def test_missing_report(self):
        """Test getting missing reports."""
        response = self.client.get('/api/1/report/missing')
        self.assertEqual(
            response.json()['results'],
            {'pipelines': serializers.PipelineSimpleSerializer(
                models.Pipeline.objects.filter(pipeline_id__in=[1001, 1002, 1003]),
                many=True).data
             }
        )

    def test_missing_report_since(self):
        """Test getting missing reports with since filter."""
        since = datetime.datetime.now() - datetime.timedelta(hours=48)

        response = self.client.get(f'/api/1/report/missing?since={since}')
        self.assertEqual(
            response.json()['results'],
            {'pipelines': serializers.PipelineSimpleSerializer(
                models.Pipeline.objects.filter(pipeline_id__in=[1001, 1002]),
                many=True).data
             }
        )

    def test_missing_report_since_with_report(self):
        """Test getting missing reports with since filter."""
        since = datetime.datetime.now() - datetime.timedelta(hours=48)

        pipeline = models.Pipeline.objects.get(pipeline_id=1001)
        pipeline.reports.create(
            body='content',
            subject='subject',
            sent_at=timezone.now(),
            msgid='<someting@redhat.com>',
        )

        response = self.client.get(f'/api/1/report/missing?since={since}')
        self.assertEqual(
            response.json()['results'],
            {'pipelines': serializers.PipelineSimpleSerializer(
                models.Pipeline.objects.filter(pipeline_id__in=[1002]),
                many=True).data
             }
        )

    def test_report_multipart(self):
        """Test creating report from multipart email."""
        pipeline = models.Pipeline.objects.first()
        email = self.load_email('email_multipart')

        self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")

        report = pipeline.reports.first()

        self.assertEqual(
            '\nHello,\n\nW',
            report.body[:10])
