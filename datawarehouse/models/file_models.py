# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to the files."""
import pathlib
from urllib.parse import urlparse

from django.db import models


class Artifact(models.Model):
    """Model for Artifact."""

    name = models.CharField(max_length=150)
    url = models.URLField(max_length=400)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def save(self, *args, **kwargs):
        # pylint: disable=arguments-differ
        """
        Override save method.

        Generate name from url if not provided.
        """
        if not self.name:
            path = urlparse(self.url).path
            self.name = pathlib.Path(path).name

        super(Artifact, self).save(*args, **kwargs)

    @classmethod
    def create_from_url(cls, url):
        """Create Artifact from url."""
        return cls.objects.get_or_create(url=url)[0]
